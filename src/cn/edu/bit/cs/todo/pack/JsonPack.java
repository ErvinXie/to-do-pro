package cn.edu.bit.cs.todo.pack;

import cn.edu.bit.cs.todo.model.AbstractEvent;
import cn.edu.bit.cs.todo.model.Event;
import cn.edu.bit.cs.todo.model.Project;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * 用于转换为前端所使用的json
 * 可以讲Projects进行递归的转化，变为前端可用的Json
 */
public class JsonPack {
    private String title;
    private String remarks;
    private Boolean isCompleted;
    ArrayList<JsonPack> subEventList = new ArrayList<>();

    JsonPack(String title, String remarks, Boolean isCompleted) {
        this.title = title;
        this.remarks = remarks;
        this.isCompleted = isCompleted;
    }

    void addSubEvent(JsonPack jsonPack){
        this.subEventList.add(jsonPack);
    }

    public static JsonPack createJsonPack(AbstractEvent event) {
        JsonPack jsonPack = new JsonPack(event.getTitle(),
                event.getRemarks(),
                event.isCompleted());
        for (AbstractEvent e : event.subEventList){
            jsonPack.subEventList.add(JsonPack.createJsonPack(e));
        }
        return jsonPack;
    }

    public static void main(String[] args) {
        Project project = new Project("project1","test project");
        project.addSubEvent(new Event("event1","today"));
        project.addSubEvent(new Event("event2","today"));


        project.addSubEvent(new Event("event3","today"));

        new Event("sube=vent1","today");

        ArrayList<JsonPack> arrayList = new ArrayList<>();

        JsonPack jsonPack = createJsonPack(project);
        arrayList.add(createJsonPack(project));
        arrayList.add(createJsonPack(project));
        System.out.println(new Gson().toJson(arrayList));
    }
}
