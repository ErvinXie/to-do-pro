package cn.edu.bit.cs.todo.pack;

import com.google.gson.Gson;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class RequestPack {

    public String getUname() {
        return uname;
    }

    public String getUpass() {
        return upass;
    }

    public ArrayList<ProjectPack> getJsonData() {
        return jsonData;
    }

    public RequestPack setUname(String uname) {
        this.uname = uname;
        return this;
    }

    public RequestPack setUpass(String upass) {
        this.upass = upass;
        return this;
    }

    public RequestPack setUemail(String uemail) {
        this.uemail = uemail;
        return this;
    }

    public RequestPack setJsonData(ArrayList<ProjectPack> jsonData) {
        this.jsonData = jsonData;
        return this;
    }

    public RequestPack setAction(String action) {
        this.action = action;
        return this;
    }

    public RequestPack setSuccess(Boolean success) {
        this.success = success;
        return this;
    }

    public RequestPack setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public RequestPack setUnameOld(String unameOld) {
        this.unameOld = unameOld;
        return this;
    }

    public RequestPack setUpassOld(String upassOld) {
        this.upassOld = upassOld;
        return this;
    }

    private String uname;
    private String upass;
    private Boolean display;

    public String getUemail() {
        return uemail;
    }

    private String uemail;
    private ArrayList<ProjectPack> jsonData;
    private String action;
    private Boolean success;
    private String msg;
    private Boolean res;

    public RequestPack setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    private LocalDateTime updateTime;

    public String getUnameOld() {
        return unameOld;
    }

    public String getUpassOld() {
        return upassOld;
    }

    private String unameOld;
    private String upassOld;

    private RequestPack(String userName, String password, String action, ArrayList<ProjectPack> jsonData, Boolean status, String message, Boolean result) {
        this.uname = userName;
        this.upass = password;
        this.action = action;
        this.jsonData = jsonData;
        this.success = status;
        this.msg = message;
        this.res = result;
    }

    public boolean isAction(String action) {
        return action.equals(this.action);
    }

    public RequestPack(String action) {
        this.action = action;
    }

    public RequestPack() {
    }

    public static RequestPack createRequestPack(String userName, String password, String action, ArrayList<ProjectPack> jsonData) {
        return new RequestPack(userName, password, action, jsonData, null, null, null);
    }

    public static RequestPack createRequestPack(String userName, String password, String action) {
        return new RequestPack(userName, password, action, null, null, null, null);
    }

    public static RequestPack createRequestPack(String userName, String email, String password, String action) {
        RequestPack requestPack = new RequestPack(userName, password, action, null, null, null, null);
        requestPack.uemail = email;
        return requestPack;
    }

    public static RequestPack createRequestPack(Boolean success, String msg) {
        return new RequestPack(null, null, null, null, success, msg, null);
    }

    public static RequestPack createRequestPack(Boolean success, Boolean result) {
        return new RequestPack(null, null, null, null, success, null, result);
    }


    public static RequestPack createRequestPack(String json) {
//        System.out.println("createRequestPack");
        RequestPack requestPack = new Gson().fromJson(json, RequestPack.class);
//        System.out.println(requestPack.toJson());
        return requestPack;
        
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public static void main(String[] args) {
        ProjectPack pp = ProjectPack.createProjectPack("sfd", "data", "data", LocalDateTime.now());
        ArrayList<ProjectPack> array = new ArrayList<>();
        array.add(pp);
        RequestPack p = RequestPack.createRequestPack("fky", "password", "take", array);
        System.out.println(p.toJson());
    }

    public Boolean getRes() {
        return res;
    }

    public void setRes(Boolean res) {
        this.res = res;
    }

    public Boolean getSuccess() {
        return success;
    }

    public String getMsg() {
        return msg;
    }

    public Boolean getDisplay() {
        return display;
    }

    public RequestPack setDisplay(Boolean display) {
        this.display = display;
        return this;
    }
}
