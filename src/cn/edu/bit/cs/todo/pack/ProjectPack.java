package cn.edu.bit.cs.todo.pack;

import cn.edu.bit.cs.todo.interfaces.Serialize;
import cn.edu.bit.cs.todo.model.Project;
import com.google.gson.Gson;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;


public class ProjectPack {
    // 打包Project类json。ProjectPack会被进而打包成一个json

    private String title;
    private String projectId;
    private String data;
    private LocalDateTime updateTime;

    public String getTitle() {
        return title;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getData() {
        return data;
    }

    private ProjectPack(String title, String projectId, String data, LocalDateTime updateTime){
        this.title = title;
        this.projectId = projectId;
        this.data = data;
        this.updateTime = updateTime;
    }

    // 采用工厂函数
    public static ProjectPack createProjectPack(String title,String projectId, String data, LocalDateTime updateTime){
        return new ProjectPack(title, projectId, data, updateTime);
    }

    // 采用工厂函数
    public static ProjectPack createProjectPack(String title, String projectId, String data) {
        return new ProjectPack(title, projectId, data, null);
    }

    // 采用工厂函数
    public static ProjectPack createProjectPack(String json) {
        return new Gson().fromJson(json, ProjectPack.class);
    }

    public static ProjectPack createProjectPack(Serialize serialize){
        return new ProjectPack(serialize.getTitle(),serialize.getProjectId(),serialize.toJson(),null);
    }

    public String toJson(){
        Gson gson = new Gson();
        String result = gson.toJson(this);
//        System.out.println(result);
        return result;
    }

    public static void main(String[] args){
        ProjectPack pack = ProjectPack.createProjectPack("title ","afseg:","dddd");
        String s = pack.toJson();
        ProjectPack pac = ProjectPack.createProjectPack(s);
        pac.toJson();
    }
}
