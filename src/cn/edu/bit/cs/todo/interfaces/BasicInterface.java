package cn.edu.bit.cs.todo.interfaces;

import cn.edu.bit.cs.todo.exceptions.LogicException;
import cn.edu.bit.cs.todo.pack.ProjectPack;

import java.util.ArrayList;

public interface BasicInterface {

    void addUser(String userName, String email, String password) throws LogicException;

    void modifyUser(String userNameOld, String passwordOld, String userNameNew, String emailNew, String passwordNew) throws LogicException;

    void saveAllProjects(String username, String password, ArrayList<ProjectPack> arrayList) throws LogicException;

    ArrayList<ProjectPack> getAllProjects(String username, String password) throws LogicException;

    boolean getDisplayStatus(String username) throws LogicException;

    void toggleDisplayStatus(String username, String password) throws LogicException;
}
