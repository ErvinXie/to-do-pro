package cn.edu.bit.cs.todo.interfaces;

public interface  Serialize {
    String toJson();
    String getProjectId();
    String getTitle();
}