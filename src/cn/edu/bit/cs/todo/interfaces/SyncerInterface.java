package cn.edu.bit.cs.todo.interfaces;

import cn.edu.bit.cs.todo.exceptions.LogicException;

/**
 * SyncerInterface 继承了 BasicInterface的基本操作
 * @see cn.edu.bit.cs.todo.interfaces.BasicInterface
 * 但是也定义了更加顶层的接口
 */
public interface SyncerInterface extends BasicInterface {
    boolean login(String usernameOrEmail, String password) throws LogicException;

    boolean logout();
}
