package cn.edu.bit.cs.todo.interfaces;

import cn.edu.bit.cs.todo.exceptions.LogicException;
import cn.edu.bit.cs.todo.pack.ProjectPack;
import cn.edu.bit.cs.todo.pack.RequestPack;

import java.time.LocalDateTime;
import java.util.ArrayList;

// 不存储用户名密码
public interface MiddleIOInterface extends BasicInterface {

    RequestPack getUser(String username, String password) throws LogicException;

    boolean verifyUser(String username, String password) throws LogicException;

    ArrayList<ProjectPack> getAllProjects(String usernameOrEmail) throws LogicException;

    LocalDateTime getProjectsUpdateTime(String username, String password) throws LogicException;

}
