package cn.edu.bit.cs.todo.controller;

import cn.edu.bit.cs.todo.exceptions.LogicException;
import cn.edu.bit.cs.todo.model.Project;
import cn.edu.bit.cs.todo.pack.ProjectPack;
import cn.edu.bit.cs.todo.sync.Syncer;
import sun.applet.Main;

import java.util.ArrayList;
import java.util.Date;
import java.util.TimerTask;

public class AutoSave extends TimerTask {
    MainEnter mainEnter;
    AutoSave(MainEnter mainEnter){
        this.mainEnter = mainEnter;
    }
    public void run(){
        mainEnter.saveData();
    }
}
