package cn.edu.bit.cs.todo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;

import cn.edu.bit.cs.todo.exceptions.LogicException;
import cn.edu.bit.cs.todo.model.Project;
import cn.edu.bit.cs.todo.pack.ProjectPack;
import cn.edu.bit.cs.todo.sync.Syncer;
import cn.edu.bit.cs.todo.utils.ColorGenerator;
import cn.edu.bit.cs.todo.utils.Constants;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class MainEnter extends Application {

	private Stage primaryStage;
	private double x = 0.00;
	private double y = 0.00;
	private double width = 0.00;
	private double height = 0.00;
	private boolean isMax = false;
	private boolean isEast = false;// 是否处于右边界调整窗口状态
	private boolean isWest = false;
	private boolean isNorth = false;
	private boolean isSouth = false;// 是否处于下边界调整窗口状态
	private boolean isSouthEast = false;// 是否处于右下角调整窗口状态
	private boolean isSouthWest = false;
	private boolean isNorthWest = false;
	private boolean isNorthEast = false;
	private double RESIZE_WIDTH = 5.00;
	private double MIN_WIDTH = 600.00;
	private double MIN_HEIGHT = 600.00;
	private double xOffset = 0, yOffset = 0;//自定义dialog移动横纵坐标
	GridPane topBar = new GridPane();
	AnchorPane holder = new AnchorPane();
	GridPane root = new GridPane();
	Color topBarColor = ColorGenerator.RandomDarkColor();
	public void initializeTopBar(GridPane root) {

		topBar.setId("topBar");

		topBar.setBackground(new Background(new BackgroundFill(topBarColor, null, null)));
		root.setBorder(new Border(new BorderStroke(topBarColor,BorderStrokeStyle.SOLID,CornerRadii.EMPTY,new BorderWidths(5.0))));
		topBar.setPrefHeight(Constants.topBarHeight);

		root.add(topBar,0,0);

		myButton buttonMin = new myButton("buttonMin", 20.0, 20.0);
//		buttonMin.setFill(Color.YELLOW);
		myButton buttonMax = new myButton("buttonMax", 20.0, 20.0);
//		buttonMax.setFill(Color.BLUE);
		myButton buttonClose = new myButton("buttonClose", 20.0, 20.0);
//		buttonClose.setFill(Color.RED);
		buttonMin.setImg("minimize.png");
		buttonMax.setImg("maxmize.png");
		buttonClose.setImg("close.png");

		Label topTitle = new Label("To Do Pro");
		topTitle.setTextFill(Color.WHITE);

		topTitle.setId("topTitle");
		topTitle.setTextAlignment(TextAlignment.LEFT);


		topBar.add(topTitle, 0, 0);
		topBar.add(buttonMin, 1, 0);
		topBar.add(buttonMax, 2, 0);
		topBar.add(buttonClose, 3, 0);
		topBar.setAlignment(Pos.CENTER);

		GridPane.setHgrow(topTitle, Priority.ALWAYS);
		topBar.setPadding(new Insets((Constants.topBarHeight - myButton.defaultButtionSize) / 2,(Constants.topBarHeight - myButton.defaultButtionSize) / 2,(Constants.topBarHeight - myButton.defaultButtionSize) / 2,10.0));
//		topBar.setGridLinesVisible(true);
		topBar.setHgap((Constants.topBarHeight - myButton.defaultButtionSize) / 2);

		//这些代码加上去按钮动画就OK了我也不知道为什么
		buttonClose.setMinHeight(20.0);
		buttonMax.setMinHeight(20.0);
		buttonMin.setMinHeight(20.0);
		buttonClose.setMinWidth(20.0);
		buttonMax.setMinWidth(20.0);
		buttonMin.setMinWidth(20.0);
//这些代码加上去按钮动画就OK了我也不知道为什么

		buttonClose.setOnMouseClicked(new EventHandler<MouseEvent>() {

			public void handle(MouseEvent event) {
				saveData();
				primaryStage.close();
			}

		});

		buttonMax.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Rectangle2D rectangle2d = Screen.getPrimary().getVisualBounds();
				isMax = !isMax;
				if (isMax) {
					// 最大化
					primaryStage.setX(rectangle2d.getMinX());
					primaryStage.setY(rectangle2d.getMinY());
					primaryStage.setWidth(rectangle2d.getWidth());
					primaryStage.setHeight(rectangle2d.getHeight());
				} else {
					// 缩放回原来的大小
					primaryStage.setX(x);
					primaryStage.setY(y);
					primaryStage.setWidth(width);
					primaryStage.setHeight(height);
				}
			}
		});

		buttonMin.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				primaryStage.setIconified(true);
			}
		});
	}

	public void initializeWindowSizeControl(GridPane root) {
		primaryStage.xProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (newValue != null && isMax == false) {
					x = newValue.doubleValue();
				}
			}
		});
		primaryStage.yProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (newValue != null && isMax == false) {
					y = newValue.doubleValue();
				}
			}
		});
		primaryStage.widthProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                topBar.setMaxWidth(newValue.doubleValue()-2*RESIZE_WIDTH);
                topBar.setMinWidth(newValue.doubleValue()-2*RESIZE_WIDTH);
				holder.setMaxWidth(newValue.doubleValue()-2*RESIZE_WIDTH);
				holder.setMinWidth(newValue.doubleValue()-2*RESIZE_WIDTH);
				loginWindow.setMaxWidth(newValue.doubleValue()-2*RESIZE_WIDTH);
				loginWindow.setMinWidth(newValue.doubleValue()-2*RESIZE_WIDTH);

//				System.out.println(newValue.doubleValue()+" but "+loginWindow.getWidth());
				if (newValue != null && isMax == false) {
					width = newValue.doubleValue();

				}
			}
		});
		primaryStage.heightProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {

				if (newValue != null && isMax == false) {
					height = newValue.doubleValue();
				}
			}
		});

		root.setOnMouseMoved((MouseEvent event) -> {
			event.consume();
			double x = event.getSceneX();
			double y = event.getSceneY();
			double width = primaryStage.getWidth();
			double height = primaryStage.getHeight();
			Cursor cursorType = Cursor.DEFAULT;// 鼠标光标初始为默认类型，若未进入调整窗口状态，保持默认类型
			// 先将所有调整窗口状态重置
			isEast = isSouth = isWest = isNorth = isNorthEast = isSouthEast = isNorthWest = isSouthWest = false;

			if (y >= height - RESIZE_WIDTH) {
				if (x <= RESIZE_WIDTH) {
					isSouthWest = true;
					cursorType = Cursor.SW_RESIZE;
				} else if (x >= width - RESIZE_WIDTH) {
					isSouthEast = true;
					cursorType = Cursor.SE_RESIZE;
				} else {
					isSouth = true;
					cursorType = Cursor.S_RESIZE;
				}
			} else if (y <= RESIZE_WIDTH) {
				if (x <= RESIZE_WIDTH) {
					isNorthWest = true;
					cursorType = Cursor.NW_RESIZE;
				} else if (x >= width - RESIZE_WIDTH) {
					isNorthEast = true;
					cursorType = Cursor.NE_RESIZE;
				} else {
					isNorth = true;
					cursorType = Cursor.N_RESIZE;
				}
			} else {
				if (x <= RESIZE_WIDTH) {
					isWest = true;
				} else if (x >= width - RESIZE_WIDTH) {
					isEast = true;
				}
			}
			// 最后改变鼠标光标
			root.setCursor(cursorType);
		});
		root.setOnMouseDragged((MouseEvent event) -> {

			//根据鼠标的横纵坐标移动dialog位置
			event.consume();
			if (yOffset != 0) {
				primaryStage.setX(event.getScreenX() - xOffset);
				if (event.getScreenY() - yOffset < 0) {
					primaryStage.setY(0);
				} else {
					primaryStage.setY(event.getScreenY() - yOffset);
				}
			}

			double x = event.getSceneX();
			double y = event.getSceneY();
			// 保存窗口改变后的x、y坐标和宽度、高度，用于预判是否会小于最小宽度、最小高度
			double nextX = primaryStage.getX();
			double nextY = primaryStage.getY();
			double nextWidth = primaryStage.getWidth();
			double nextHeight = primaryStage.getHeight();
			if (isEast || isSouthEast || isNorthEast) {// 所有右边调整窗口状态
				nextWidth = x;
			}
			if (isSouth || isSouthEast || isSouthWest) {// 所有下边调整窗口状态
				nextHeight = y;
			}
			if (isWest || isSouthWest || isNorthWest) {// 所有左边调整窗口状态
				if (nextWidth >= MIN_WIDTH) {
					nextWidth = nextWidth - x;
					nextX = x + nextX;
				}
			}
			if (isNorth || isNorthWest || isNorthEast) {// 所有上边调整窗口状态
				if (nextHeight >= MIN_HEIGHT) {
					nextHeight = nextHeight - y;
					nextY = y + nextY;
				}
			}
			if (nextWidth <= MIN_WIDTH) {// 如果窗口改变后的宽度小于最小宽度，则宽度调整到最小宽度
				nextWidth = MIN_WIDTH;
			}
			if (nextHeight <= MIN_HEIGHT) {// 如果窗口改变后的高度小于最小高度，则高度调整到最小高度
				nextHeight = MIN_HEIGHT;
			}
			// 最后统一改变窗口的x、y坐标和宽度、高度，可以防止刷新频繁出现的屏闪情况
			primaryStage.setX(nextX);
			primaryStage.setY(nextY);
			primaryStage.setWidth(nextWidth);
			primaryStage.setHeight(nextHeight);

		});

		root.setOnMousePressed(event -> {
			event.consume();
			xOffset = event.getSceneX();
			if (event.getSceneY() > Constants.topBarHeight || event.getSceneY() <= RESIZE_WIDTH) {
				yOffset = 0;
			} else {
				yOffset = event.getSceneY();
			}
		});

		topBar.setOnMouseClicked(event -> {
			if (event.getClickCount() == 2) {
				primaryStage.setX(Constants.defaultX);
				primaryStage.setY(Constants.defaultY);
				primaryStage.setWidth(Constants.defaultWidth);
				primaryStage.setHeight(Constants.defaultHeight);
				isMax = false;
			}
		});
	}


	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage = primaryStage;
		mainWindow();
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}








	void saveData() {

		Runnable task = ()->
		{
//			Syncer syncer = new Syncer(loginWindow.getAddress(), loginWindow.getPort(), "./", "todo_pro.dbs");
			try {
				syncer.login(userNameOrEmail, password);
			} catch (LogicException e) {
				e.printStackTrace();
				e.getMsg();
				return;
			}
			ArrayList<ProjectPack> arrayList = new ArrayList<>();
			for (Project p : controller.projectArrayList) {
				arrayList.add(ProjectPack.createProjectPack(p));
			}
			try {
				syncer.saveAllProjects(arrayList);
				System.out.println("Save OK " + new Date());
			} catch (LogicException e) {
				e.printStackTrace();
				return;
			}


		};
		new Thread(task).start();
	}
	void setShare() throws LogicException {
		syncer.toggleDisplayStatus(userNameOrEmail,password);

	}
	boolean getShareStatus() throws LogicException {
		return syncer.getDisplayStatus(userNameOrEmail);
	}

	String userNameOrEmail;
	String password;

	public void setSyncer(Syncer syncer) {
		this.syncer = syncer;
	}

	Syncer syncer = null;


	Timer timer = new Timer();

	public String logIn(String userNameOrEmail,String password) {
		String response =  controller.getProjects(userNameOrEmail,password,syncer);
		if(response=="OK"){
			this.userNameOrEmail = userNameOrEmail;
			this.password = password;
			timer =  new Timer();
			timer.schedule(new AutoSave(this),1000,30000);
		}
		return response;
	}
	public void logOut() {

		holder.getChildren().clear();
		controller.initializeProjectList();
		syncer.logout();
		timer.cancel();
	}
	MainWindowController controller = null;
	LoginWindow loginWindow= new LoginWindow(root, this);

	void mainWindow() {
		syncer = loginWindow.getSyncer();
		System.out.println("GOT");
		root.setId("root");
		holder.setId("root");

		holder.setStyle("-fx-border-radius: " + RESIZE_WIDTH + "px");
		holder.setBackground(new Background(new BackgroundFill(Color.GRAY, null, null)));

		root.setPrefHeight(Constants.defaultHeight);
		root.setPrefWidth(Constants.defaultWidth);
		root.add(holder,0,2);

		root.add(loginWindow,0,1);


		root.heightProperty().addListener((observable, oldValue, newValue) -> {
			holder.setMaxHeight(newValue.doubleValue()-Constants.topBarHeight-Constants.loginBarHeight-RESIZE_WIDTH*2);
			holder.setMinHeight(newValue.doubleValue()-Constants.topBarHeight-Constants.loginBarHeight-RESIZE_WIDTH*2);
		});



		controller = new MainWindowController(holder);

		controller.setMainEnter(this);

		initializeTopBar(root);
		initializeWindowSizeControl(root);
		Scene scene = new Scene(root);


		primaryStage.initStyle(StageStyle.TRANSPARENT);

		primaryStage.setScene(scene);


		primaryStage.setTitle("to-do-pro");
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
