package cn.edu.bit.cs.todo.controller;

import cn.edu.bit.cs.todo.exceptions.LogicException;
import cn.edu.bit.cs.todo.model.Project;
import cn.edu.bit.cs.todo.pack.ProjectPack;
import cn.edu.bit.cs.todo.sync.Syncer;
import com.google.gson.Gson;
import javafx.scene.layout.AnchorPane;

import java.util.ArrayList;


public class MainWindowController {
	MainEnter mainEnter;

	private AnchorPane holder;
	public void setMainEnter(MainEnter mainEnter){
		this.mainEnter = mainEnter;
	}


	MainWindowController(AnchorPane holder){
		this.holder = holder;
		initializeProjectList();
	}

	public ArrayList<Project> projectArrayList = new ArrayList<Project>();
	public myProjectListView projectListView = new myProjectListView(this);

	public void  addProject(Project project){
		projectArrayList.add(project);
		project.mainWindowController = this;
		project.holder = holder ;
		projectListView.refresh();
		System.out.println("New Project["+project.getTitle()+"] created");
	}


	String getProjects(String userNameOrEmail,String password,Syncer syncer){

		if(projectArrayList==null){
			projectArrayList = new ArrayList<Project>();
		}
		else{
			projectArrayList.clear();
		}
		if(projectListView == null){
			projectListView = new myProjectListView(this);
		}

		this.projectArrayList.clear();
		ArrayList<ProjectPack> allProjects = new ArrayList<>();

		try {
			boolean login = syncer.login(userNameOrEmail, password);
			if(login==false) {
				return "登陆失败";
			}
		} catch (LogicException e) {
			e.printStackTrace();
			return e.getMsg();
		}
		try {
			allProjects = syncer.getAllProjects();
		} catch (LogicException e) {
			e.printStackTrace();
			return e.getMsg();
		}

		for(ProjectPack pp:allProjects){
			Project newProject = new Gson().fromJson(pp.getData(),Project.class);
			newProject.holder=this.holder;
			newProject.setTransient();
			newProject.mainWindowController = this;
			this.projectArrayList.add(newProject);
		}
		projectListView.refresh();
		return "OK";
	}


	public void initializeProjectList() {
		holder.getChildren().add(projectListView);
	}
}
