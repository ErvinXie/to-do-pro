package cn.edu.bit.cs.todo.controller;

import cn.edu.bit.cs.todo.utils.ColorGenerator;
import cn.edu.bit.cs.todo.utils.Constants;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;

public class myButton extends HBox {
    public static final double defaultButtionSize = 20.0;

    String name;
    Image image = new Image("/cn/edu/bit/cs/todo/icon/blank.png",100.0,100.0,false,false);
    Color borderColor = Color.WHITE;

    ImageView imageView = new ImageView(image);
    Label title = new Label();
    public myButton(String name, Double width, Double height) {
        super();
        this.name = name;
        showName();
        this.setId(name);
        setColor();

        this.setPrefSize(width, height);

        this.setAlignment(Pos.CENTER);


//        System.out.println(this.getWidth());
        imageView.setPreserveRatio(true);
        imageView.setFitWidth(width);
        imageView.setFitHeight(height);


        this.widthProperty().addListener((observable, oldValue, newValue) -> {
            imageView.setFitWidth(newValue.doubleValue());
        });
        this.heightProperty().addListener((observable, oldValue, newValue) -> {
            imageView.setFitHeight(newValue.doubleValue());
        });


        this.getChildren().add(imageView);


        this.setBorder(new Border(new BorderStroke(Color.WHITE,BorderStrokeStyle.SOLID,CornerRadii.EMPTY,new BorderWidths(0.0))));
        this.setOnMouseEntered(event -> {
            double tar=Math.min(this.getHeight(),this.getWidth());
            tar = Math.max(2.5,tar*0.075);
            changeBorder(tar,100);
            changeContent(0.90,100);
        });
        this.setOnMouseExited(event -> {
            changeBorder(0.0,100);
            changeContent(1.0,100);
        });


//        this.setOnMouseReleased(event -> {
//            changeBorder(0,100);
//            changeContent(1.0,100);
//        });
        initializeLoading();

    }

    void setText(String t){
        this.getChildren().clear();
        title.setText(t);
        title.setFont(new Font(20.0));
        title.setTextFill(Color.WHITE);
        this.getChildren().add(title);
    }

    void setImg(String url){
        try {
            image = new Image("/cn/edu/bit/cs/todo/icon/" + url, 100.0, 100.0, true, false);
        }
        catch (Exception  e){

        }
        imageView.setImage(image);
    }

    void setImg(String url,boolean kp){
        try {
            image = new Image("/cn/edu/bit/cs/todo/icon/" + url, 100.0, 100.0, true, false);
            imageView.setPreserveRatio(kp);
        }
        catch (Exception  e){

        }
        imageView.setImage(image);
    }

    void showName(){
//        this.getChildren().add(new Label(this.name));
    }

    void setColor(){
        this.setBackground(new Background(new BackgroundFill(ColorGenerator.RandomMiddleColor(),null,null)));
    }
    void setColor(Color color){
        this.setBackground(new Background(new BackgroundFill(color,null,null)));
    }
    final Timeline loadingTimeline = new Timeline();
    void initializeLoading() {
        double totalTime = 1000;

        double tar=Math.min(this.getHeight(),this.getWidth());
        tar = Math.max(2.5,tar*0.075);
        double width = tar;
        double widthStartValue = this.getBorder().getInsets().getBottom(), widthEndValue = width;
        SimpleDoubleProperty timeChange = new SimpleDoubleProperty();
        timeChange.setValue(0.0);

        loadingTimeline.setCycleCount(Timeline.INDEFINITE);
        loadingTimeline.setCycleCount(1);

        int step = Constants.animationStep;
        for (int i = 1; i <= step; i++) {
            double time = totalTime / step * i;

            double k = (totalTime / 2) * Math.sin(Math.PI * (time) / totalTime - Math.PI / 2.0) + totalTime / 2;

            final KeyValue kv = new KeyValue(timeChange, k);
            final KeyFrame kf = new KeyFrame(Duration.millis(time), kv);
//            System.out.println(time+" - "+k);
            loadingTimeline.getKeyFrames().addAll(kf);
        }

        timeChange.addListener((observable, oldValue, newValue) -> {
            double ratio = newValue.doubleValue() / totalTime;

            this.setBorder(new Border(new BorderStroke(borderColor,
                    BorderStrokeStyle.SOLID,
                    CornerRadii.EMPTY, new BorderWidths(widthStartValue + (widthEndValue - widthStartValue) * ratio))));

        });
        loadingTimeline.setAutoReverse(true);
    }
    public void setLoading(boolean go){
        if(go) {
            loadingTimeline.play();//运行
        }
        else{
            loadingTimeline.stop();
        }
    }



    void changeBorder(double width,double totalTime) {
        double widthStartValue = this.getBorder().getInsets().getBottom(), widthEndValue = width;
        SimpleDoubleProperty timeChange = new SimpleDoubleProperty();
        timeChange.setValue(0.0);
        final Timeline timeline = new Timeline();
//        loadingTimeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setCycleCount(1);

        int step = Constants.animationStep;
        for (int i = 1; i <= step; i++) {
            double time = totalTime / step * i;

            double k = (totalTime / 2) * Math.sin(Math.PI * (time) / totalTime - Math.PI / 2.0) + totalTime / 2;

            final KeyValue kv = new KeyValue(timeChange, k);
            final KeyFrame kf = new KeyFrame(Duration.millis(time), kv);
//            System.out.println(time+" - "+k);
            timeline.getKeyFrames().addAll(kf);
        }

        timeChange.addListener((observable, oldValue, newValue) -> {
            double ratio = newValue.doubleValue() / totalTime;

            this.setBorder(new Border(new BorderStroke(borderColor,
                    BorderStrokeStyle.SOLID,
                    CornerRadii.EMPTY, new BorderWidths(widthStartValue + (widthEndValue - widthStartValue) * ratio))));

        });
        timeline.setAutoReverse(true);
        timeline.play();//运行
    }



    void changeContent(double scale,double totalTime){

        double widthStartValue = title.getScaleX(),widthEndValue = scale;
        SimpleDoubleProperty timeChange = new SimpleDoubleProperty();
        timeChange.setValue(0.0);
        final Timeline timeline=new Timeline();
//        loadingTimeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setCycleCount(1);

        int step = Constants.animationStep;
        for(int i=1;i<=step;i++){
            double time = totalTime/step*i;

            double k=(totalTime/2)*Math.sin(Math.PI*(time)/totalTime-Math.PI/2.0)+totalTime/2;

            final KeyValue kv=new KeyValue(timeChange,k);
            final KeyFrame kf=new KeyFrame(Duration.millis(time), kv);
//            System.out.println(time+" - "+k);
            timeline.getKeyFrames().addAll(kf);
        }


        timeChange.addListener((observable, oldValue, newValue) -> {
            double ratio = newValue.doubleValue()/totalTime;
            double kv = widthStartValue+(widthEndValue-widthStartValue)*ratio;
            title.setScaleY(kv);
            title.setScaleX(kv);
            imageView.setScaleY(kv);
            imageView.setScaleX(kv);
        });
        timeline.setAutoReverse(true);
        timeline.play();//运行

    }


}
