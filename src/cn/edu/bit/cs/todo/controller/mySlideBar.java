package cn.edu.bit.cs.todo.controller;

import cn.edu.bit.cs.todo.utils.Constants;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class mySlideBar extends AnchorPane {
    AnchorPane Bar ;
    GridPane showPane;
    mySlideBar that =  this;
    double width, holderHeight,totalHeight;

    double startX,startY;

    boolean isMoving = false;
    mySlideBar(double width,GridPane showPane) {
        super();
        this.width=width;
        this.setWidth(width);
        this.showPane = showPane;
        this.setBackground(new Background(new BackgroundFill(Color.DARKGRAY,null,null)));

        Bar = new AnchorPane();
        Bar.setPrefWidth(width);
        Bar.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY,null,null)));
//        Bar.setBackground(new Background(new BackgroundFill(Color.RED,null,null)));
        this.getChildren().add(Bar);

        Bar.layoutYProperty().addListener((observable, oldValue, newValue) -> {
//            System.out.println(-newValue.doubleValue()/that.holderHeight*that.totalHeight);
            AnchorPane.setTopAnchor(that.showPane,-newValue.doubleValue()/that.holderHeight*that.totalHeight);
        });

        Bar.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                event.consume();
                setCursor(Cursor.HAND);
            }
        });


        Bar.setOnMouseDragReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                event.consume();
                isMoving = false;
            }
        });


        Bar.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                event.consume();
                isMoving = true;
                startY = event.getSceneY();
            }
        });
        Bar.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                event.consume();

                double x = event.getSceneX();
                double y = event.getSceneY();
                // 保存窗口改变后的x、y坐标和宽度、高度，用于预判是否会小于最小宽度、最小高度
                double nextY = Bar.getLayoutY()+y-startY;

//                System.out.println(Bar.getLayoutY()+" "+nextY+" "+isMoving+" "+nextY+Bar.getHeight()+" "+that.holderHeight);
                if(nextY>=0.0&&nextY+Bar.getHeight()<=that.holderHeight&&isMoving){
//                    System.out.println(nextY);
                    that.Bar.setLayoutY(nextY);
                }
                startY = y;
            }
        });
    }


    public void setColor(Color color){
        Bar.setBackground(new Background(new BackgroundFill(color,null,null)));
    }

    public void setRatio(double holderHeight,double totalHeight) {
        this.holderHeight =holderHeight;
        this.totalHeight = totalHeight;
//        System.out.println(holderHeight + " "+totalHeight);

        this.setPrefHeight(holderHeight);
        this.setLayoutY(0.0);

        if(holderHeight>totalHeight){
//            Bar.setPrefHeight(holderHeight);
            moveTo(Bar.getLayoutY(),holderHeight,Constants.slideBarBounceTime);
        }else{
//            Bar.setPrefHeight(holderHeight*holderHeight/totalHeight);
            moveTo(Bar.getLayoutY(),holderHeight*holderHeight/totalHeight,Constants.slideBarBounceTime);
        }

    }





    void moveTo(double y,double height,double totalTime){

        double yStartValue = Bar.getLayoutY(),yEndValue = y;
        double heightStartValue = Bar.getHeight(),heightEndValue = height;

        SimpleDoubleProperty timeChange = new SimpleDoubleProperty();
        timeChange.setValue(0.0);

        final Timeline timeline=new Timeline();

        timeline.setCycleCount(1);

        int step = Constants.animationStep;
        for(int i=1;i<=step;i++){
            double time = totalTime/step*i;

            double k=(totalTime/2)*Math.sin(Math.PI*(time)/totalTime-Math.PI/2.0)+totalTime/2;

            final KeyValue kv=new KeyValue(timeChange,k);
            final KeyFrame kf=new KeyFrame(Duration.millis(time), kv);
//            System.out.println(time+" - "+k);
            timeline.getKeyFrames().addAll(kf);
        }
        timeChange.addListener((observable, oldValue, newValue) -> {
            double ratio = newValue.doubleValue()/totalTime;
            Bar.setPrefHeight(heightStartValue+(heightEndValue-heightStartValue)*ratio);
            Bar.setLayoutY(yStartValue+(yEndValue-yStartValue)*ratio);
        });

        timeline.setAutoReverse(true);
        timeline.play();//运行
    }
}
