package cn.edu.bit.cs.todo.controller;

import cn.edu.bit.cs.todo.model.AbstractEvent;
import cn.edu.bit.cs.todo.model.Event;
import cn.edu.bit.cs.todo.utils.ColorGenerator;
import cn.edu.bit.cs.todo.utils.Constants;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;

import java.util.ArrayList;

import javafx.scene.paint.Color;
import javafx.util.Duration;
import sun.reflect.annotation.AnnotationSupport;

public class myEventListView extends AnchorPane {

    myEventListView that = this;
    AbstractEvent father;
    ArrayList<Event> eventArrayList;


    GridPane showPane = new GridPane();
    GridPane buttons = new GridPane();





    mySlideBar slideBar = new mySlideBar(Constants.slideBarWidth,showPane);
    public myEventListView(AbstractEvent father){
        super();
        System.out.println("["+father.getTitle()+"]'s subevent list created");

        this.father = father;
        eventArrayList = father.subEventList;

        this.setLayoutX(0.0);
        this.setLayoutY(0.0);
        this.setMaxWidth(Constants.eventBarWidth);
        this.setMinWidth(Constants.eventBarWidth);
        AnchorPane.setTopAnchor(this,0.0);
        AnchorPane.setBottomAnchor(this,0.0);


        AnchorPane.setTopAnchor(showPane,0.0);
        AnchorPane.setLeftAnchor(showPane,0.0);

        showPane.widthProperty().addListener((observable, oldValue, newValue) -> {
            for (int i = 0; i < eventArrayList.size(); i++) {
                eventArrayList.get(i).getEventView().setMaxWidth(newValue.doubleValue());
                eventArrayList.get(i).getEventView().setMinWidth(newValue.doubleValue());
            }
        });


        this.widthProperty().addListener((observable, oldValue, newValue) -> {
//            System.out.println("showpane"+newValue.doubleValue());
            showPane.setMaxWidth(newValue.doubleValue()-Constants.slideBarWidth);
            showPane.setMinWidth(newValue.doubleValue()-Constants.slideBarWidth);
        });

        AnchorPane.setTopAnchor(slideBar,0.0);
        AnchorPane.setRightAnchor(slideBar,0.0);
//        this.setBorder(new Border(new BorderStroke(Color.CORAL,BorderStrokeStyle.DASHED,CornerRadii.EMPTY,new BorderWidths(5.0))));
//        showPane.setBorder(new Border(new BorderStroke(Color.YELLOW,BorderStrokeStyle.SOLID,CornerRadii.EMPTY,new BorderWidths(2.0))));

        initializeButtons();
        initializeSlideBar();

    }

    myButton addevent = new myButton("new project",this.getPrefWidth(),Constants.buttonHeight);
    myButton move = new myButton("move",this.getPrefWidth(),Constants.buttonHeight);
    myButton showOrhideCompleted = new myButton("showOrhideCompleted",this.getPrefWidth(),Constants.buttonHeight);

    SimpleBooleanProperty isShowingCompleted = new SimpleBooleanProperty(false);

    void initializeButtons(){
        buttons.add(addevent,0,1);
        buttons.add(move,0,3);
        buttons.add(showOrhideCompleted,0,2);

        addevent.setImg("listadd.png");
        move.setImg("push.png");
        showOrhideCompleted.setImg("dontShowCompleted.png");
        addevent.setMinHeight(50.0);
        move.setMinHeight(50.0);
        showOrhideCompleted.setMinHeight(50.0);//不知道为什么加上这句话按钮的特效就正常了

        showPane.widthProperty().addListener((observable, oldValue, newValue) -> {
            buttons.setMaxWidth(newValue.doubleValue());
            buttons.setMinWidth(newValue.doubleValue());
        });
//        buttons.setBorder(new Border(new BorderStroke(Color.RED,BorderStrokeStyle.DASHED,CornerRadii.EMPTY,BorderWidths.DEFAULT)));

        buttons.widthProperty().addListener((observable, oldValue, newValue) -> {
            addevent.setMaxWidth(newValue.doubleValue());
            move.setMaxWidth(newValue.doubleValue());
            addevent.setMinWidth(newValue.doubleValue());
            move.setMinWidth(newValue.doubleValue());
//            showOrhideCompleted.setMaxWidth(newValue.doubleValue());
//            showOrhideCompleted.setMinWidth(newValue.doubleValue());
        });



        AnchorPane.setLeftAnchor(buttons,0.0);
        AnchorPane.setRightAnchor(buttons,0.0);



        addevent.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                father.addSubEvent(new Event());
            }
        });

        move.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                that.fold();
            }
        });

        showOrhideCompleted.setOnMouseClicked(event -> {
            isShowingCompleted.setValue(!isShowingCompleted.getValue());
            refresh();
        });

        isShowingCompleted.addListener((observable, oldValue, newValue) -> {
            if(newValue.booleanValue()==true){
                showOrhideCompleted.setImg("showCompleted.png");
            }
            else{
                showOrhideCompleted.setImg("dontShowCompleted.png");
            }
        });

    }

    public void initializeSlideBar(){
        that.heightProperty().addListener((observable, oldValue, newValue) -> {
            slideBar.setRatio(newValue.doubleValue(),showPane.getHeight());
        });

        showPane.heightProperty().addListener((observable, oldValue, newValue) -> {
            slideBar.setRatio(that.getHeight(),newValue.doubleValue());
        });
    }


    public void refresh() {

        double x = 0;
        if(father != null){
//            System.out.println(father.toString());
            this.setPrefHeight(father.holder.getHeight());
            x = father.getRightX();
        }

        this.setLayoutX(x);
        this.setLayoutY(0.0);

        this.getChildren().clear();
        showPane.getChildren().clear();


        int cnt=0;
        for (int i = 0; i < eventArrayList.size(); i++) {
//            eventArrayList.get(i).eventView.refresh();
            if(isShowingCompleted.getValue()==true||eventArrayList.get(i).isCompleted()==false){
                showPane.add(eventArrayList.get(i).getEventView(),0, cnt);
                cnt++;
            }
        }
        showPane.add(buttons, 0, cnt++);


//        System.out.println(this.getHeight());

        this.getChildren().add(showPane);
        this.getChildren().add(slideBar);
        GridPane.setVgrow(slideBar, Priority.ALWAYS);

        System.out.println("["+father.getTitle()+"]'s subevent list refreshed");
    }



    boolean isFolded = false;
    public void foldBackwards(int dep){
        for(int i=0;i<father.subEventList.size();i++){
            if(father.subEventList.get(i).subEventListView == null){
                father.subEventList.get(i).initializeListView();
            }
            father.subEventList.get(i).subEventListView.foldBackwards(dep);
        }
        if(dep<father.getDepth()) {
            unfold(dep);
        }
    }
    void foldFront(){
        if(father!=null&&father.father!=null){
            father.father.subEventListView.foldFront();
        }
        moveTo(Constants.foldedEventBarWidth*father.getDepth()+Constants.projectBarWidth,Constants.foldedEventBarWidth,Constants.barMoveTime);
        for(int i=0;i<father.subEventList.size();i++){
            father.subEventList.get(i).getEventView().foldlist();
        }
    }

    void fold(){
        foldFront();
        foldBackwards(father.getDepth());
    }

    public void unfold(int dep){
//        System.out.println(dep);
        for(int i=0;i<father.subEventList.size();i++){
            father.subEventList.get(i).getEventView().unfoldlist();
        }
        moveTo(Constants.projectBarWidth+Constants.foldedEventBarWidth*(dep+1)+Constants.eventBarWidth*(father.getDepth()-dep-1), Constants.eventBarWidth,Constants.barMoveTime);
        isFolded = false;

    }



    public void hide(){
        moveToY(-this.getPrefHeight(),this.getPrefHeight(),1000.0);
    }

    void moveToY(double y,double height,double totalTime){
        double yStartValue = this.getLayoutY(),yEndValue = y;
        double heightStartValue = this.getHeight(),heightEndValue = height;

        SimpleDoubleProperty timeChange = new SimpleDoubleProperty();
        timeChange.setValue(0.0);
        final Timeline timeline=new Timeline();
//        loadingTimeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setCycleCount(1);

        int step = Constants.animationStep;
        for(int i=1;i<=step;i++){
            double time = totalTime/step*i;

            double k=(totalTime/2)*Math.sin(Math.PI*(time)/totalTime-Math.PI/2.0)+totalTime/2;

            final KeyValue kv=new KeyValue(timeChange,k);
            final KeyFrame kf=new KeyFrame(Duration.millis(time), kv);
//            System.out.println(time+" - "+k);
            timeline.getKeyFrames().addAll(kf);
        }


        timeChange.addListener((observable, oldValue, newValue) -> {
            double ratio = newValue.doubleValue()/totalTime;

            this.setPrefHeight(heightStartValue+(heightEndValue-heightStartValue)*ratio);
            this.setLayoutY(yStartValue+(yEndValue-yStartValue)*ratio);
        });
        timeline.setAutoReverse(true);
        timeline.play();//运行
    }

    void moveTo(double x,double width,double totalTime){

        double xStartValue = this.getLayoutX(),xEndValue = x;
        double widthStartValue = this.getWidth(),widthEndValue = width;

        SimpleDoubleProperty timeChange = new SimpleDoubleProperty();
        timeChange.setValue(0.0);
        final Timeline timeline=new Timeline();
//        loadingTimeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setCycleCount(1);

        int step = Constants.animationStep;
        for(int i=1;i<=step;i++){
            double time = totalTime/step*i;

            double k=(totalTime/2)*Math.sin(Math.PI*(time)/totalTime-Math.PI/2.0)+totalTime/2;

            final KeyValue kv=new KeyValue(timeChange,k);
            final KeyFrame kf=new KeyFrame(Duration.millis(time), kv);
//            System.out.println(time+" - "+k);
            timeline.getKeyFrames().addAll(kf);
        }


        timeChange.addListener((observable, oldValue, newValue) -> {
            double ratio = newValue.doubleValue()/totalTime;
            this.setMaxWidth(widthStartValue+(widthEndValue-widthStartValue)*ratio);
            this.setMinWidth(widthStartValue+(widthEndValue-widthStartValue)*ratio);
            this.setLayoutX(xStartValue+(xEndValue-xStartValue)*ratio);
        });
        timeline.setAutoReverse(true);
        timeline.play();//运行
    }


}
