package cn.edu.bit.cs.todo.controller;

import cn.edu.bit.cs.todo.exceptions.LogicException;
import cn.edu.bit.cs.todo.sync.Syncer;
import cn.edu.bit.cs.todo.utils.ColorGenerator;
import cn.edu.bit.cs.todo.utils.Constants;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class LoginWindow extends GridPane {
    GridPane root;
    MainEnter mainEnter;
    LoginWindow that = this;
    public SimpleBooleanProperty isFold = new SimpleBooleanProperty(false);

    public SimpleBooleanProperty isLogedIn = new SimpleBooleanProperty(false);

    GridPane buttons = new GridPane();
    myButton foldButton = new myButton("fol", 50.0, 50.0);

    GridPane logIn = new GridPane();
    TextField userName = new TextField();
    TextField email = new TextField();
    PasswordField passwordField = new PasswordField();
    myButton logInButton = new myButton("login", 300.0, 50.0);
    myButton signInButton = new myButton("singIn", 300.0, 50.0);
    Label errMsg = new Label();

    Text title = new Text();

    LoginWindow(GridPane root, MainEnter mainEnter) {
        super();
//        userName.setText("test");
//        passwordField.setText("testpass");
        this.setBackground(new Background(new BackgroundFill(ColorGenerator.RandomMiddleDarkColor(), null, null)));
        this.root = root;
        this.mainEnter = mainEnter;

        errMsg.setTextFill(Color.RED);
        errMsg.setFont(new Font(20.0));
        syncer = new Syncer(address.getText(), new Integer(port.getText()), "./", "todo_pro.dbs");


        root.heightProperty().addListener((observable, oldValue, newValue) -> {
            if (isFold.getValue() == true) {
                this.setMinHeight(Constants.loginBarHeight);
                this.setMaxHeight(Constants.loginBarHeight);
            } else {
                this.setMinHeight(root.getHeight() - Constants.topBarHeight);
            }
        });




        DropShadow dropShadow = new DropShadow(1.0, 0.0, 0.0, Color.BLACK);
        dropShadow.setBlurType(BlurType.GAUSSIAN);
        this.setEffect(dropShadow);

        initializeButtons();
        initializeLogIn();
        initializeFolded();
        initializeServer();
        this.add(title, 0, 0);

        isFold.setValue(false);
        isFold.addListener((observable, oldValue, newValue) -> {
            if (newValue == true) {
                fold();
            } else {
                unfold();
            }
        });
    }


    void initializeLogIn() {

        title.setText("To Do Pro");
        title.setFont(new Font(30));
        title.setFill(Color.WHITE);


        logIn.setAlignment(Pos.CENTER);

        Label accoutLable = new Label("Account");
        accoutLable.setTextFill(Color.WHITE);

        Label passLable = new Label("Password");
        passLable.setTextFill(Color.WHITE);
        logIn.add(accoutLable, 0, 1);
        logIn.add(passLable, 0, 2);

        logIn.add(userName, 1, 1);
        logIn.add(passwordField, 1, 2);

        logIn.add(logInButton, 0, 3);
        logIn.add(signInButton, 0, 4);

        logIn.add(errMsg, 0, 6);

        logIn.add(setServer,0,7);
        errMsg.setVisible(false);

        GridPane.setColumnSpan(errMsg, 2);
        GridPane.setColumnSpan(signInButton, 2);
        GridPane.setColumnSpan(logInButton, 2);
        logInButton.setText("LOG IN");
        signInButton.setText("SIGN IN");

        userName.setMinWidth(300.0);
        userName.setMaxWidth(300.0);
        userName.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode().equals(KeyCode.ENTER)) {
                    passwordField.requestFocus();
                }
            }
        });

        passwordField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode().equals(KeyCode.ENTER)) {
                    doLogIn();
                }
            }
        });


        signInButton.setMinHeight(50.0);
        signInButton.setMinWidth(300.0);

        signInButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                doSignIn();
            }
        });

        logInButton.setMinHeight(50.0);
        logInButton.setMinWidth(300.0);
        logInButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                doLogIn();
            }
        });


        logIn.setVgap(20.0);
        logIn.setHgap(10.0);
        this.add(logIn, 0, 1);


        logging.addListener((observable, oldValue, newValue) -> {
//            logInButton.setLoading(newValue.booleanValue());
        });

    }
    SimpleBooleanProperty logging = new SimpleBooleanProperty(false);

    void doLogIn() {


        logging.setValue(true);
        String response = mainEnter.logIn(userName.getText(), passwordField.getText());
        if (response == "OK") {
            fold();

        } else {
            errMsg.setText(response);
            errMsg.setVisible(true);
        }
        logging.setValue(false);
    }

    public Syncer getSyncer() {
        return syncer;
    }

    Syncer syncer = null;

    void doSignIn() {
        logIn.getChildren().remove(logInButton);

        logIn.add(logInButton,0,4);
        logIn.getChildren().remove(signInButton);
        logIn.add(signInButton,0,5);
        try {
            Label emailTitle = new Label("E-mail");
            emailTitle.setTextFill(Color.WHITE);
            logIn.add(emailTitle, 0, 3);
            logIn.add(email, 1, 3);
        } catch (Exception e) {
            ;
        }

        if (userName.getText() == "") {
            errMsg.setText("请输入用户名");
            return;
        }
        if (email.getText() == "") {
            errMsg.setText("请输入邮箱地址");
            return;
        }
        if (passwordField.getText().length() < 6) {
            errMsg.setText("密码长度小于6位");
            return;
        }

        try {
            syncer.addUser(userName.getText(), email.getText(), passwordField.getText());
            doLogIn();
        } catch (LogicException e) {
            e.printStackTrace();
            errMsg.setVisible(true);
            errMsg.setText(e.getMsg());
        }

    }

    void doLogOut(){
        unfold();
        mainEnter.saveData();
        mainEnter.logOut();
        email.setText("");
        userName.setText("");
        passwordField.setText("");
        mainEnter.controller.projectArrayList.clear();
    }

    void initializeButtons() {
        buttons.add(foldButton, 0, 0);

        foldButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (isFold.getValue() == true) {
                    isFold.setValue(false);
                } else {
                    isFold.setValue(true);
                }
            }
        });


        this.setAlignment(Pos.CENTER);
        buttons.setAlignment(Pos.CENTER);
        foldButton.autosize();

    }

    public String getAddress() {
        return address.getText();
    }
    public Integer getPort(){
        return new Integer(port.getText());
    }
    TextField address = new TextField("59.110.233.235");
    TextField port = new TextField("8899");
    TextField sharePort = new TextField("8500");
    myButton buttonSetServer = new myButton("setServer",100.0,Constants.loginBarHeight);
    GridPane setServer = new GridPane();
    SimpleBooleanProperty isSettingServer = new SimpleBooleanProperty(false);

    void initializeServer(){

        setServer.add(buttonSetServer,0,3);
        GridPane.setColumnSpan(buttonSetServer,2);
        GridPane.setColumnSpan(setServer,2);
        GridPane.setHgrow(setServer,Priority.ALWAYS);
        logIn.widthProperty().addListener((observable, oldValue, newValue) -> {
            buttonSetServer.setMinWidth(newValue.doubleValue());
        });

        buttonSetServer.setText("SET SERVER");
        setServer.setHgap(5.0);
        setServer.setVgap(5.0);
        buttonSetServer.setOnMouseClicked(event -> {
            isSettingServer.setValue(!isSettingServer.getValue());
        });
        buttonSetServer.setMinHeight(50.0);


        isSettingServer.addListener((observable, oldValue, newValue) -> {
            if(newValue.booleanValue()==true){
                Label addressT = new Label("Address");
                addressT.setTextFill(Color.WHITE);
                Label portT = new Label("Port");
                portT.setTextFill(Color.WHITE);

                setServer.add(addressT,0,1);
                setServer.add(portT,1,1);
                setServer.add(address,0,2);
                setServer.add(port,1,2);
            }else {
                syncer = new Syncer(address.getText(), new Integer(port.getText()), "./", "todo_pro.dbs");
                mainEnter.setSyncer(syncer);
                setServer.getChildren().clear();
                setServer.add(buttonSetServer,0,3);
                System.out.println("New Server Set "+address.getText()+":"+port.getText());
            }
        });
    }
    GridPane foldedView = new GridPane();

    myButton save = new myButton("save", 100.0, Constants.loginBarHeight);
    myButton logOut = new myButton("logout",100.0,Constants.loginBarHeight);
    myButton setShare = new myButton("setShare",100.0,Constants.loginBarHeight);
    SimpleBooleanProperty isSharing = new SimpleBooleanProperty(false);

    void initializeFolded() {
        this.widthProperty().addListener((observable, oldValue, newValue) -> {
            foldedView.setMaxWidth(newValue.doubleValue());
            foldedView.setMinWidth(newValue.doubleValue());
        });
        save.setImg("save.png");
        logOut.setImg("logout.png");
        foldedView.setPadding(new Insets(0, 0, 0, 10));

        //加上这句话按钮动画显示就正常了，我也不知道为什么
        save.setMinHeight(Constants.loginBarHeight);
        logOut.setMinHeight(Constants.loginBarHeight);
        setShare.setMinHeight(Constants.loginBarHeight);
        //以上

        save.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mainEnter.saveData();
            }
        });
        logOut.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                doLogOut();
            }
        });
//        foldedView.setGridLinesVisible(true);
//        this.setGridLinesVisible(true);
        GridPane.setHgrow(userName2, Priority.ALWAYS);
        userName2.setFill(Color.WHITE);
        foldedView.add(userName2, 0, 0);
        foldedView.add(save, 2, 0);
        foldedView.add(logOut, 3, 0);

        foldedView.add(setShare, 1, 0);
        setShare.setImg("complete.png");

        isSharing.addListener((observable, oldValue, newValue) -> {
            if (newValue.booleanValue() == true) {
                setShare.setImg("completed.png");
                userName2.setText(userName2.getText() + "   ShareLink: " + address.getText() + ":" + sharePort.getText() + "/" + userName.getText() +
                        " has been put in your clipboard!");

                ClipboardContent clipboardContent = new ClipboardContent();
                clipboardContent.putString(address.getText() + ":" + sharePort.getText() + "/" + userName.getText());
                Clipboard.getSystemClipboard().setContent(clipboardContent);


//                System.out.println("iss com");
            } else {
                setShare.setImg("complete.png");
                userName2.setText(userName.getText());
//                System.out.println("iss decom");
            }

            try {
                if (newValue.booleanValue() != mainEnter.getShareStatus()) {
                    try {
                        mainEnter.setShare();
                    } catch (LogicException e) {
                        e.printStackTrace();
                    }
                }
            } catch (LogicException e) {
                e.printStackTrace();
            }

        });

        setShare.setOnMouseReleased(event -> {
            isSharing.setValue(!isSharing.getValue());
        });
    }

    Text userName2 = new Text();

    void fold() {
        isFold.set(true);
        moveToY(this.getLayoutY(), Constants.topBarHeight, 500);
        this.getChildren().clear();
        this.add(foldedView, 0, 0);
        userName2.setText(userName.getText());
        try {
            isSharing.setValue(mainEnter.getShareStatus());
            System.out.println("isSharing " + mainEnter.getShareStatus());
        } catch (LogicException e) {
            e.printStackTrace();
        }
    }

    void unfold() {
        moveToY(this.getLayoutY(), root.getHeight() - Constants.topBarHeight, 500);
        this.getChildren().clear();
        this.add(title, 0, 0);
        this.add(logIn, 0, 1);
        errMsg.setVisible(false);
        isFold.set(false);
    }


    void moveToY(double y, double height, double totalTime) {
        double yStartValue = this.getLayoutY(), yEndValue = y;
        double heightStartValue = this.getHeight(), heightEndValue = height;

        SimpleDoubleProperty timeChange = new SimpleDoubleProperty();
        timeChange.setValue(0.0);
        final Timeline timeline = new Timeline();
//        loadingTimeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setCycleCount(1);

        int step = Constants.animationStep;
        for (int i = 1; i <= step; i++) {
            double time = totalTime / step * i;

            double k = (totalTime / 2) * Math.sin(Math.PI * (time) / totalTime - Math.PI / 2.0) + totalTime / 2;

            final KeyValue kv = new KeyValue(timeChange, k);
            final KeyFrame kf = new KeyFrame(Duration.millis(time), kv);
//            System.out.println(time+" - "+k);
            timeline.getKeyFrames().addAll(kf);
        }


        timeChange.addListener((observable, oldValue, newValue) -> {
            double ratio = newValue.doubleValue() / totalTime;
            this.setMaxHeight(heightStartValue + (heightEndValue - heightStartValue) * ratio);
            this.setMinHeight(heightStartValue + (heightEndValue - heightStartValue) * ratio);
            this.setLayoutY(yStartValue + (yEndValue - yStartValue) * ratio);
        });
        timeline.setAutoReverse(true);
        timeline.play();//运行
    }

}
