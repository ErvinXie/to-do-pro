package cn.edu.bit.cs.todo.controller;

import cn.edu.bit.cs.todo.model.Project;
import cn.edu.bit.cs.todo.utils.Constants;
import com.sun.media.sound.ModelOscillator;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;

public class myProjectListView extends AnchorPane {

    myProjectListView that = this;
    MainWindowController mainWindowController = null;
    ArrayList<Project> projectArrayList;

    GridPane showPane = new GridPane();
    GridPane buttons = new GridPane();


    mySlideBar slideBar = new mySlideBar(Constants.slideBarWidth,showPane);

    public myProjectListView(MainWindowController mainWindowController) {
        super();
        this.mainWindowController = mainWindowController;
        this.projectArrayList = mainWindowController.projectArrayList;
        this.setLayoutX(0.0);
        this.setLayoutY(0.0);
        this.setPrefWidth(Constants.projectBarWidth);
        AnchorPane.setTopAnchor(this, 0.0);
        AnchorPane.setBottomAnchor(this, 0.0);

        AnchorPane.setLeftAnchor(showPane, 0.0);
        AnchorPane.setRightAnchor(showPane, Constants.slideBarWidth);


        AnchorPane.setTopAnchor(slideBar, 0.0);
        AnchorPane.setRightAnchor(slideBar, 0.0);

        initializeButtons();
        initializeSlideBar();
    }


    myButton addproj = new myButton("new proj",this.getPrefWidth(),Constants.buttonHeight);
    myButton unfoldAll = new myButton("unfoldAll",this.getPrefWidth(),Constants.buttonHeight);
    myButton showOrhideCompleted = new myButton("showOrhideCompleted",this.getPrefWidth(),Constants.buttonHeight);

    SimpleBooleanProperty isShowingCompleted = new SimpleBooleanProperty(false);
    void initializeButtons(){
        buttons.add(addproj,0,0);
        buttons.add(unfoldAll,0,2);
        buttons.add(showOrhideCompleted,0,1);
        addproj.setImg("listadd.png");
        unfoldAll.setImg("push.png");
        showOrhideCompleted.setImg("dontShowCompleted.png");
        addproj.setMinHeight(50.0);
        unfoldAll.setMinHeight(50.0);
        showOrhideCompleted.setMinHeight(50.0);//不知道为什么加上这句话按钮的特效就正常了


        this.widthProperty().addListener((observable, oldValue, newValue) -> {
            unfoldAll.setPrefWidth(newValue.doubleValue());
            addproj.setPrefWidth(newValue.doubleValue());
        });

        unfoldAll.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                that.foldBackwards(-1);
            }
        });
        addproj.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mainWindowController.addProject(new Project());
            }
        });
        showOrhideCompleted.setOnMouseClicked(event -> {
            isShowingCompleted.setValue(!isShowingCompleted.getValue());
            refresh();
        });
        isShowingCompleted.addListener((observable, oldValue, newValue) -> {
            if(newValue.booleanValue()==true){
                showOrhideCompleted.setImg("showCompleted.png");
            }
            else{
                showOrhideCompleted.setImg("dontShowCompleted.png");
            }
        });
    }

    void initializeSlideBar(){
        that.heightProperty().addListener((observable, oldValue, newValue) -> {
            slideBar.setRatio(newValue.doubleValue(),showPane.getHeight());
        });

        showPane.heightProperty().addListener((observable, oldValue, newValue) -> {
            slideBar.setRatio(that.getHeight(),newValue.doubleValue());
        });
    }

    public void refresh() {
        this.getChildren().clear();
        showPane.getChildren().clear();
//        System.out.println(projectArrayList.toString());
        int cnt=0;
        for (int i = 0; i < projectArrayList.size(); i++) {
//            eventArrayList.get(i).eventView.refresh();
            if(isShowingCompleted.getValue()==true||projectArrayList.get(i).isCompleted()==false){
                showPane.add(projectArrayList.get(i).getProjectView(),0, cnt);
                cnt++;
            }
        }
        showPane.add(buttons, 0, cnt++);

        this.getChildren().add(showPane);
        this.getChildren().add(slideBar);
    }

    void foldBackwards(int dep){
        for(int i=0;i<projectArrayList.size();i++){
            if(projectArrayList.get(i).subEventListView == null){
                projectArrayList.get(i).initializeListView();
            }
            projectArrayList.get(i).subEventListView.foldBackwards(dep);
        }
    }
}
