package cn.edu.bit.cs.todo.controller;

import cn.edu.bit.cs.todo.model.Event;
import cn.edu.bit.cs.todo.utils.ColorGenerator;
import cn.edu.bit.cs.todo.utils.Constants;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class myEventView extends GridPane {

    Event event;

    myEventView that = this;

    Label title = new Label();
    TextField titleEdit = new TextField();

    Text remarks = new Text();
    TextArea remarksEdit = new TextArea();

    GridPane details = new GridPane();
    GridPane summary = new GridPane();

    SimpleBooleanProperty isEditingTitle = new SimpleBooleanProperty(false);
    SimpleBooleanProperty isEditingRemarks = new SimpleBooleanProperty(false);
    SimpleBooleanProperty isShowingDetails = new SimpleBooleanProperty(false);
    SimpleBooleanProperty isShowingList = new SimpleBooleanProperty(false);
    SimpleBooleanProperty isCompleted = new SimpleBooleanProperty(false);


    public myEventView(Event event) {
        super();
        initializeStyle();
        this.setBackground(new Background(new BackgroundFill(
                ColorGenerator.getGrey(deCompletedGrey),
                null,
                null)));
        System.out.println("EventView of [" + event.getTitle() + "] created");
        this.event = event;
        summary.setPadding(new Insets(0, 0, 0, 20.0));
        title.setText(event.getTitle());
        titleEdit.setText(event.getTitle());
        remarks.setText(event.getRemarks());
        remarksEdit.setText(event.getRemarks());

        title.setFont(new Font(20.0));
        titleEdit.setFont(new Font(20.0));
        this.widthProperty().addListener((observable, oldValue, newValue) -> {
//            System.out.println(newValue.doubleValue());
            summary.setMaxWidth(newValue.doubleValue());
            summary.setMinWidth(newValue.doubleValue());
            details.setMaxWidth(newValue.doubleValue());
            details.setMinWidth(newValue.doubleValue());

        });

        summary.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                if (event.getX() > complete.getLayoutX()) {
                    return;
                }
                if(that.event.getTitle().trim().isEmpty()==true){
                    isEditingTitle.setValue(true);
                }
//                if(project.getX()>title.getLayoutX()
//                        &&project.getX()<title.getLayoutX()+title.getWidth()
//                        &&project.getY()>title.getLayoutY()
//                        &&project.getY()<title.getLayoutY()+title.getHeight()){
//                    return;
//                }
                if (isShowingDetails.getValue() == true) {
                    isShowingDetails.setValue(false);
                } else {
                    isShowingDetails.setValue(true);
                }
            }
        });


        summary.widthProperty().addListener((observable, oldValue, newValue) -> {
//            complete.setPrefWidth(newValue.doubleValue() / 4);
            complete.setMaxWidth(newValue.doubleValue() / 4);
            complete.setMinWidth(newValue.doubleValue() / 4);

        });
//        summary.setBorder(new Border(new BorderStroke(Color.TRANSPARENT, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(Constants.detailsBorderWitdh))));

        summary.setMinHeight(Constants.eventBarHeight);
        summary.add(title, 0, 0);
        summary.add(complete, 1, 0);

        GridPane.setHgrow(title, Priority.ALWAYS);
        GridPane.setHgrow(titleEdit, Priority.ALWAYS);
        this.setMinHeight(Constants.eventBarHeight);
        this.add(summary, 0, 0);

        title.setText(event.getTitle());
        remarks.setText(event.getRemarks());
        remarks.wrappingWidthProperty().bind(details.widthProperty().subtract(buttonEditRemarks.widthProperty()));
        remarks.wrappingWidthProperty().bind(buttonEditRemarks.widthProperty().multiply(3));
        initializeButtons();
        initializeDetails();

    }

    void initializeStyle() {
        DropShadow dropShadow = new DropShadow(3.0, 0.0, 0.0, Color.BLACK);
        dropShadow.setBlurType(BlurType.GAUSSIAN);
        this.setEffect(dropShadow);
        details.setEffect(dropShadow);
    }

    myButton addSubEvent = new myButton("add", Constants.eventBarButtonHeight, Constants.eventBarHeight);
    myButton delete = new myButton("cpl", Constants.eventBarButtonHeight, Constants.eventBarHeight);
    myButton complete = new myButton("com", Constants.eventBarButtonHeight, Constants.eventBarHeight);
    //    myButton showOrHideDetails = new myButton("hid",Constants.eventBarButtonHeight,Constants.eventBarHeight);
    myButton showOrHideSublist = new myButton("sho", Constants.eventBarButtonHeight, Constants.eventBarHeight);
    myButton buttonEditRemarks = new myButton("FE", Constants.eventBarButtonHeight, Constants.eventBarHeight);


    void initializeButtons() {

        addSubEvent.setImg("addsub.png");

        addSubEvent.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                that.event.addSubEvent(new Event());
                isShowingList.setValue(true);
            }
        });


        complete.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                isCompleted.setValue(!isCompleted.getValue());
            }
        });

        showOrHideSublist.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                isShowingList.setValue(!isShowingList.getValue());
            }
        });



        delete.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                that.event.delete();
            }
        });
        delete.setImg("delete.png");
        delete.setColor(Color.CORAL);
        isShowingDetails.addListener((observable, oldValue, newValue) -> {
//            System.out.println(newValue.booleanValue());
            if (newValue.booleanValue() == true) {
                unfold();
            } else {
                fold();
            }
        });
        showOrHideSublist.setImg("showlist.png");

        isShowingList.addListener((observable, oldValue, newValue) -> {
            if (newValue.booleanValue() == true) {
                that.event.showList();
                that.event.father.subEventListView.slideBar.setColor(detailColor);
                showOrHideSublist.setImg("hidelist.png");
            } else {
                that.event.hideList();
                showOrHideSublist.setImg("showlist.png");
            }
        });


        complete.setImg("complete.png");
        complete.setMinHeight(summary.getHeight());
        isCompleted.addListener((observable, oldValue, newValue) -> {
            if (newValue.booleanValue() == true) {
                that.event.complete();
                title.setTextFill(Color.GRAY);
                complete.setImg("completed.png");
//                summary.setBackground(new Background(new BackgroundFill(ColorGenerator.getGrey(deCompletedGrey),null,null)));
                changeColor(deCompletedGrey,completedGrey,100);
            } else {
                that.event.deComplete();
                title.setTextFill(Color.BLACK);
                complete.setImg("complete.png");
                changeColor(completedGrey,deCompletedGrey,100);
//                summary.setBackground(new Background(new BackgroundFill(ColorGenerator.getGrey(completedGrey),null,null)));
            }
            that.event.father.subEventListView.refresh();
        });
        isCompleted.setValue(that.event.isCompleted());

    }

    Color detailColor = ColorGenerator.RandomLightColor();
    void initializeDetails() {

//        details.setBorder(new Border(new BorderStroke(Color.TRANSPARENT, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(Constants.detailsBorderWitdh))));
        details.setPadding(new Insets(Constants.detailsVgap, 0, Constants.detailsVgap, 0));
        details.setVgap(Constants.detailsVgap);
        details.setMinHeight(100.0);
        details.setBackground(new Background(new BackgroundFill(detailColor, null, null)));

        titleEdit.textProperty().addListener((observable, oldValue, newValue) -> {
            event.setTitle(newValue.toString());
            that.title.setText(newValue.toString());
        });

        titleEdit.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.booleanValue() == false) {
                that.isEditingTitle.set(false);
            }
        });
        that.title.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                event.consume();
                titleEdit.setText(that.event.getTitle());
                isEditingTitle.set(true);
            }
        });

        titleEdit.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode().equals(KeyCode.ENTER)) {
                    isEditingTitle.set(false);
                }
            }
        });

        isEditingTitle.addListener((observable, oldValue, newValue) -> {
            if (newValue.booleanValue() == true) {
                summary.getChildren().remove(title);
                titleEdit.setText(event.getTitle());
                summary.add(titleEdit, 0, 0);
                titleEdit.requestFocus();
            } else {
                that.event.setTitle(titleEdit.getText());
                event.setTitle(titleEdit.getText());
                summary.getChildren().remove(titleEdit);
                summary.add(title, 0, 0);
            }
        });

        isEditingRemarks.addListener((observable, oldValue, newValue) -> {
            if (newValue.booleanValue() == true) {
                details.getChildren().remove(remarks);
                remarksEdit.setText(event.getRemarks());
                details.add(remarksEdit, 0, 0);
                remarksEdit.requestFocus();
                title.setText(event.getTitle());
                remarks.setText(event.getRemarks());
            } else {
                event.setRemarks(remarksEdit.getText());
                details.getChildren().remove(remarksEdit);
                details.add(remarks, 0, 0);
                title.setText(event.getTitle());
                remarks.setText(event.getRemarks());
            }
        });

        that.remarks.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                remarksEdit.setText(that.event.getRemarks());
                isEditingRemarks.set(true);
            }
        });


        remarksEdit.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.booleanValue() == false) {
                isEditingRemarks.set(false);
            }
        });

        buttonEditRemarks.setImg("editremark.png");
        buttonEditRemarks.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (isEditingRemarks.getValue() == true) {
                    isEditingRemarks.set(false);
                } else {
                    isEditingRemarks.set(true);
                }
            }
        });


//        buttonEditRemarks.setMaxHeight(Constants.eventBarButtonHeight);
        buttonEditRemarks.setMinHeight(Constants.eventBarButtonHeight);

//        addSubEvent.setMaxHeight(Constants.eventBarButtonHeight);
        addSubEvent.setMinHeight(Constants.eventBarButtonHeight);

        showOrHideSublist.setMinHeight(Constants.eventBarButtonHeight);
//        showOrHideSublist.setMaxHeight(Constants.eventBarButtonHeight);


//        delete.setMaxHeight(Constants.eventBarButtonHeight);
        delete.setMinHeight(Constants.eventBarButtonHeight);

        details.widthProperty().addListener((observable, oldValue, newValue) -> {
            buttonEditRemarks.setMaxWidth(newValue.doubleValue() / 4);
            buttonEditRemarks.setMinWidth(newValue.doubleValue() / 4);
            showOrHideSublist.setMaxWidth(newValue.doubleValue() / 4);
            showOrHideSublist.setMinWidth(newValue.doubleValue() / 4);
            addSubEvent.setMaxWidth(newValue.doubleValue() / 4);
            addSubEvent.setMinWidth(newValue.doubleValue() / 4);

            details.setMinWidth(newValue.doubleValue());
            details.setMaxWidth(newValue.doubleValue());
            delete.setMinWidth(newValue.doubleValue());// - 2 * Constants.detailsBorderWitdh);
            delete.setMaxWidth(newValue.doubleValue());// - 2 * Constants.detailsBorderWitdh);

        });

        GridPane.setHgrow(remarks, Priority.ALWAYS);
        GridPane.setHgrow(remarks, Priority.ALWAYS);
        GridPane.setRowSpan(remarks, 3);
        GridPane.setRowSpan(remarksEdit, 3);
//        details.setGridLinesVisible(true);


        details.add(remarks, 0, 0);

        details.add(showOrHideSublist, 1, 1);
        details.add(addSubEvent, 1, 2);
        details.add(buttonEditRemarks, 1, 0);
        details.add(delete, 0, 3);
        GridPane.setColumnSpan(delete, 2);


    }


    public void fold() {

        this.getChildren().remove(details);
    }

    public void unfold() {
        try {
            this.add(details, 0, 1);
        } catch (Exception e) {

        }
    }


    public void foldlist() {

//        this.getChildren().remove(details);
    }

    public void unfoldlist() {
//        try {
//            this.add(details, 0, 1);
//        }
//        catch (Exception e){
//
//        }
    }
    double completedGrey = ColorGenerator.rand(0.5,0.6);
    double deCompletedGrey = ColorGenerator.rand(0.8,0.9);

    void changeColor(double w,double width,double totalTime){
        
        double widthStartValue = w,widthEndValue = width;

        SimpleDoubleProperty timeChange = new SimpleDoubleProperty();
        timeChange.setValue(0.0);
        final Timeline timeline=new Timeline();
//        loadingTimeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setCycleCount(1);

        int step = Constants.animationStep;
        for(int i=1;i<=step;i++){
            double time = totalTime/step*i;

            double k=(totalTime/2)*Math.sin(Math.PI*(time)/totalTime-Math.PI/2.0)+totalTime/2;

            final KeyValue kv=new KeyValue(timeChange,k);
            final KeyFrame kf=new KeyFrame(Duration.millis(time), kv);
//            System.out.println(time+" - "+k);
            timeline.getKeyFrames().addAll(kf);
        }


        timeChange.addListener((observable, oldValue, newValue) -> {
            double ratio = newValue.doubleValue()/totalTime;
            summary.setBackground(new Background(new BackgroundFill(ColorGenerator.getGrey(widthStartValue+(widthEndValue-widthStartValue)*ratio),null,null)));
//            this.setMaxWidth(widthStartValue+(widthEndValue-widthStartValue)*ratio);
        });
        timeline.setAutoReverse(true);
        timeline.play();//运行
    }


}
