package cn.edu.bit.cs.todo.sync;

import cn.edu.bit.cs.todo.dbio.MiddleIO;
import cn.edu.bit.cs.todo.dbio.SQLiteIO;
import cn.edu.bit.cs.todo.exceptions.LogicException;
import cn.edu.bit.cs.todo.interfaces.SyncerInterface;
import cn.edu.bit.cs.todo.pack.ProjectPack;
import cn.edu.bit.cs.todo.pack.RequestPack;
import cn.edu.bit.cs.todo.socket.RemoteIO;

import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * 被前端代码直接调用，
 * 实现了最基本的操作
 * 具体实现本地和远程的数据库同步调度算法
 * 但对上层隐藏本地与远程的IO
 * 注意整个操作分为两大类，
 * 一是对账号的登入登出，（除了登入登出）这一部分必须保证远程IO可用
 * 二是对Projects的操作，只要本地数据库存在就可以使用
 * 这里所有的函数都会在调用时自动进行同步，因此不需要特别调用同步方法
 *
 * @author Kaiyu Feng
 * @version 1.0.0
 * @see cn.edu.bit.cs.todo.interfaces.SyncerInterface
 */
public class Syncer implements SyncerInterface {
    private String username;
    private String password;
    private boolean localLogIn;
    private boolean remoteLogIn;
    private RemoteIO remoteIO;
    private MiddleIO localIO;


    /**
     * @param hostName 远程服务端的hostname
     * @param port 远程端口
     * @param localpath 本地sqlitedb路径 一般是当前路径或者某个固定路径
     * @param fileName 本地sqlitedb文件名（数据库名）一般是<pre>todo_pro.db</pre>
     * 初始化本地和远程IO
     */
    public Syncer(String hostName, int port,String localpath,String fileName) {
        remoteIO = new RemoteIO(hostName, port);
        localIO = new MiddleIO(SQLiteIO.createSQLiteIO("", ""
                , localpath
                , "sqlite", fileName));
    }

    /**
     * 利用test接口进行远程链接测试，会改变remoteLogIn 状态
     */
    private void testConnect() {
        try {
            String retString = remoteIO.sendToRemote(new RequestPack("testTESTtest").toJson());
            if(retString==null){
                // 网络不可达时会返回空字符串
                remoteLogIn = false;
                return;
            }
            RequestPack.createRequestPack(retString);
        } catch (Exception e) {
            e.printStackTrace();
            remoteLogIn = false;
            return;
        }
        remoteLogIn = true;
    }

    /**
     * 从远程抓取用户信息
     */
    private void fetchUserInfo() {
        if (remoteLogIn) {
            RequestPack user;
            try {
                user = remoteIO.getUser(this.username, this.password);
            } catch (LogicException e) {
                // 网络连接问题
                remoteLogIn = false;
                return;
            }
            try {
                boolean b = localIO.verifyUser(user.getUname(), user.getUpass());
                if (b) {
                    return;
                }

                localIO.addUser(user.getUname(), user.getUemail(), user.getUpass());
            } catch (LogicException e) {
                e.printStackTrace();
                localLogIn = false;
                return;
            }
            localLogIn = true;
        }

    }

    private void syncProjectsData() {
        if (remoteLogIn && localLogIn) {

            LocalDateTime remoteUpdateTime;
            try {
                remoteUpdateTime = remoteIO.getProjectsUpdateTime(this.username, this.password);
            } catch (LogicException e) {
                e.printStackTrace();
                remoteLogIn = false;
                return;
            }
            LocalDateTime localUpdateTime;
            try {
                localUpdateTime = localIO.getProjectsUpdateTime(this.username, this.password);
            } catch (LogicException e) {
                e.printStackTrace();
                localLogIn = false;
                return;
            }
            if (localUpdateTime.isAfter(remoteUpdateTime)) {
                // 如果本地比较新
                ArrayList<ProjectPack> allProjects;
                try {
                    allProjects = localIO.getAllProjects(this.username, this.password);
                } catch (LogicException e) {
                    e.printStackTrace();
                    localLogIn = false;
                    return;
                }

                try {
                    remoteIO.saveAllProjects(this.username, this.password, allProjects);
                } catch (LogicException e) {
                    e.printStackTrace();
                    remoteLogIn = true;
                }
            } else if (localUpdateTime.isBefore(remoteUpdateTime)) {

                ArrayList<ProjectPack> allProjects;
                try {
                    allProjects = remoteIO.getAllProjects(this.username, this.password);
                } catch (LogicException e) {


                    e.printStackTrace();
                    remoteLogIn = false;
                    return;
                }
                try {
                    localIO.saveAllProjects(this.username, this.password, allProjects);
                } catch (LogicException e) {
                    e.printStackTrace();
                    localLogIn = false;
                }
            }

        }
    }

    /**
     * 登录需要满足
     * 如果远程登录成功 登录，并与本地同步
     *
     * @param usernameOrEmail username or email
     * @param password        password
     * @return if successfully LoginWindow
     */
    @Override
    public boolean login(String usernameOrEmail, String password) throws LogicException {
        boolean b;

        b = remoteIO.verifyUser(usernameOrEmail, password);


        // 如果用户远程验证成功
        if (b) {
            // 保存本地信息
            this.username = usernameOrEmail;
            this.password = password;
            this.remoteLogIn = true;

            try {
                boolean b2 = localIO.verifyUser(usernameOrEmail, password);
                if (!b2) {
                    fetchUserInfo();
                } else {
                    localLogIn = true;
                }
                // 此时都成功了
//                fetchUserInfo();
                syncProjectsData();

            } catch (LogicException e) {
                e.printStackTrace();
            }
        } else {
            // 远程失败，验证本地
            boolean b2 = localIO.verifyUser(usernameOrEmail, password);
            if (b2) {
                // 本地存在
                // 存信息
                this.username = usernameOrEmail;
                this.password = password;

                localLogIn = true;
                remoteLogIn = false;
                return true;
            } else {
                localLogIn = false;
                remoteLogIn = false;
                return false;
            }
        }
        return true;
    }

    /**
     * 无论如何都会退出
     *
     * @return 用于表示在调用函数前是否已经登出
     */
    @Override
    public boolean logout() {
        password = null;
        username = null;
        if (localLogIn || remoteLogIn) {
            localLogIn = false;
            remoteLogIn = false;
            return true;

        } else {
            return false;
        }
    }


    /**
     * 只有远程成功本地才嫩进行操作
     * 事先需要网络连通
     *
     * @param username username
     * @param email email
     * @param password password
     * @throws LogicException can't LoginWindow to remote server
     */
    @Override
    public void addUser(String username, String email, String password) throws LogicException {
        if (!remoteLogIn)
            testConnect();
        if (!remoteLogIn) {
            throw new LogicException("无法连接远程服务器，无法修改用户信息");
        } else {
            remoteIO.addUser(username, email, password);
            localIO.addUser(username, email, password);
        }
    }

    @Override
    public void modifyUser(String userNameOld, String passwordOld, String userNameNew, String emailNew, String passwordNew) throws LogicException {
        if (!remoteLogIn)
            testConnect();
        if (!remoteLogIn) {
            throw new LogicException("无法连接远程服务器，无法修改用户信息");
        } else {
            remoteIO.modifyUser(userNameOld, passwordOld, userNameNew, emailNew, passwordNew);
            localIO.modifyUser(userNameOld, passwordOld, userNameNew, emailNew, passwordNew);
        }
    }

    public void saveAllProjects(ArrayList<ProjectPack> arrayList) throws LogicException {
        if(remoteLogIn|| localLogIn){
            saveAllProjects(this.username,this.password,arrayList);
        }
        else throw new LogicException("没有登录");
    }

    /**
     * 如果本地可用就存在本地，并且尝试同步远程
     *
     * @param username 用户名或者邮箱
     * @param password  密码
     * @param arrayList projectPack list
     * @see ProjectPack
     * @throws LogicException 底层逻辑异常
     */
    @Override
    public void saveAllProjects(String username, String password, ArrayList<ProjectPack> arrayList) throws LogicException {
        if (!remoteLogIn) {
            testConnect();
        }
        if (localLogIn) {
            localIO.saveAllProjects(username, password, arrayList);
        }
        if (remoteLogIn) {
            remoteIO.saveAllProjects(username, password, arrayList);
        }

    }

    /**
     * login后可以使用这个不需要传输账号的方法
     * @return ArrayList of ProjectPack
     * @throws LogicException
     */
    public ArrayList<ProjectPack> getAllProjects() throws LogicException {
        if(remoteLogIn||localLogIn){
            return getAllProjects(this.username,this.password);
        }
        else throw new LogicException("没有登录");

    }

    /**
     * 同时提取并比较，如果远程不可用就使用本地的。
     * 如果已经login，推荐使用不带参数版本
     * @param username 账号
     * @param password 密码
     * @return ProjectPack
     * @throws LogicException 底层的逻辑异常
     */
    @Override
    public ArrayList<ProjectPack> getAllProjects(String username, String password) throws LogicException {

        if (!remoteLogIn) {
            testConnect();
        }
        ArrayList<ProjectPack> localAllProjects = null;
        LocalDateTime localProjectsUpdateTime = null;

        ArrayList<ProjectPack> remoteAllProjects = null;
        LocalDateTime remoteProjectsUpdateTime = null;

        if (localLogIn) {
            localProjectsUpdateTime = localIO.getProjectsUpdateTime(this.username, this.password);
            localAllProjects = localIO.getAllProjects(this.username, this.password);
        }
        if (remoteLogIn) {
            remoteAllProjects = remoteIO.getAllProjects(this.username, this.password);
            remoteProjectsUpdateTime = remoteIO.getProjectsUpdateTime(this.username, this.password);
        }
        if (localLogIn && remoteLogIn) {
            syncProjectsData();
            assert localProjectsUpdateTime != null;
            if (localProjectsUpdateTime.isAfter(remoteProjectsUpdateTime)) {
                return localAllProjects;
            } else {
                return remoteAllProjects;
            }
        } else if (remoteLogIn) {
            return remoteAllProjects;
        } else if (localLogIn) {
            return localAllProjects;
        } else {
            testConnect();
        }

        return new ArrayList<>();
    }

    @Override
    public boolean getDisplayStatus(String username) throws LogicException {
        if(!remoteLogIn)
            testConnect();
        if (!remoteLogIn) {
            throw new LogicException("无法连接远程服务器，无法获取用户信息");
        } else {
            return remoteIO.getDisplayStatus(username);
        }
    }

    /**
     * @return 是否打开分享状态
     * @throws LogicException 逻辑异常
     */
    public boolean getDisplayStatus() throws LogicException {
        if(!remoteLogIn)
            testConnect();
        if (!remoteLogIn) {
            throw new LogicException("无法连接远程服务器，无法获取用户信息");
        } else {
            return remoteIO.getDisplayStatus(username);
        }
    }


    @Override
    public void toggleDisplayStatus(String username, String password) throws LogicException {
        if(!remoteLogIn)
            testConnect();
        if(!remoteLogIn){
            throw new LogicException("无法连接远程服务器，无法修改展示状态");
        }
        else{
            remoteIO.toggleDisplayStatus(username,password);
        }
    }

    /**
     * 无需输入用户名密码版本
     * @throws LogicException 逻辑异常
     */
    public void toggleDisplayStatus() throws LogicException {
        if(!remoteLogIn)
            testConnect();
        if(!remoteLogIn){
            throw new LogicException("无法连接远程服务器，无法修改展示状态");
        }
        else{
            remoteIO.toggleDisplayStatus(username,password);
        }
    }


    public static void main(String[] args) {
        Syncer syncer = new Syncer("59.110.233.235", 8899,"/home/fky/code/git/mine/to-do-pro/database","todo_pro.db");
        try {
//            System.out.println(syncer.remoteIO.getProjectsUpdateTime("test", "testpass"));
//            System.out.println(syncer.localIO.getProjectsUpdateTime("test", "testpass"));
            syncer.login("test", "testpass");
//            syncer.syncProjectsData();
            syncer.getAllProjects("tset","testpass");
        } catch (LogicException e) {
            e.printStackTrace();
        }

    }
}
