package cn.edu.bit.cs.todo.server;

import cn.edu.bit.cs.todo.basictools.BasicTools;
import cn.edu.bit.cs.todo.dbio.MariadbIO;
import cn.edu.bit.cs.todo.dbio.MiddleIO;
import cn.edu.bit.cs.todo.exceptions.LogicException;
import cn.edu.bit.cs.todo.pack.JsonPack;
import cn.edu.bit.cs.todo.pack.ProjectPack;
import com.google.gson.Gson;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;

public class HttpHtmlServer {

    private static MiddleIO dbio;
    public final static String version = "v0.9.8";
    public static HashMap<String, String> fileMap = new HashMap<>();
    /**
     * local debugging -> prefix = "frontend";
     * remote deploy -> prefix = "";
     */
    private static String prefix = "";

    static String getName(URI requestURI) {
        return requestURI.getPath().substring(1);
    }

    private static void handleRequest(HttpExchange exchange) throws IOException {

        String username = getName(exchange.getRequestURI());

        String response = "";

//        System.out.println(System.getProperty("user.dir"));
        System.out.println("username["+username+"]username");
        try {
            if (username.trim().isEmpty() || username==null) {
                response = fileMap.get("default.html");
                System.out.println("default");
            } else {
                boolean displayStatus = dbio.getDisplayStatus(username);
                if (displayStatus == false) {
                    response = fileMap.get("forbid.html");
                } else {
                    if (username.endsWith(".css")) {
                        response = fileMap.get(username);
                    } else if (username.endsWith(".js")) {
                        response = fileMap.get(username);
                    } else {
                        response = fileMap.get("index.html").replace("USERUSER", username);
                    }
                }
            }

        } catch (LogicException e) {
//            e.printStackTrace();
            if (username.endsWith(".css")) {
                response = fileMap.get(username);
            } else if (username.endsWith(".js")) {
                response = fileMap.get(username);
            } else {
                response = "not permitted";
            }
        }

        exchange.sendResponseHeaders(200, response.getBytes().length);//response code and length
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

    private static void handleJsonRequest(HttpExchange exchange) throws IOException {

        String username = getName(exchange.getRequestURI()).substring(5);
        String response;
        try {
            boolean displayStatus = dbio.getDisplayStatus(username);
            response = String.valueOf(displayStatus);
            response = response == null ? response : "[{\"title\":\"You are not allowed to read\",\"remarks\":\"\",\"subEventList\":[]}]";
            if (displayStatus == true) {
                ArrayList<ProjectPack> arrayList = dbio.getAllProjects(username);
                ArrayList<JsonPack> jsonPackArrayList = new ArrayList<>();

                for (ProjectPack projectPack : arrayList) {

                    JsonPack jsonPack = new Gson().fromJson(projectPack.getData(), JsonPack.class);
                    jsonPackArrayList.add(jsonPack);
                }
//                arrayList.forEach((e) -> {
//                    jsonPackArrayList.add(JsonPack.createJsonPack(new Gson().fromJson(e.getData(), Project.class)));
//                });

                response = new Gson().toJson(jsonPackArrayList);
            }
        } catch (LogicException e) {
            e.printStackTrace();
            System.out.println(e.getMsg());

            response = "{\"status\":\"error\"}";
        }
        exchange.sendResponseHeaders(200, response.getBytes().length);//response code and length
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

    public static void main(String[] args) throws IOException, IOException, LogicException {

        String user = BasicTools.getEnvConfig("DATABASE_USER", "root");
        String password = BasicTools.getEnvConfig("DATABASE_PASSWORD", "passwordW");
        String host = BasicTools.getEnvConfig("DATABASE_HOSTNAME", "database");
        int port = BasicTools.getEnvConfig("DATABASE_PORT", 3306);
        String databaseName = BasicTools.getEnvConfig("DATABASE_NAME", "todo_pro");

        // comment this when publish
//        host = "59.110.233.235";

        dbio = new MiddleIO(new MariadbIO(user, password, host, port, "mariadb", databaseName));
        int localPort = BasicTools.getEnvConfig("Local_PORT", 8500);

        fileMap.put("index.html", BasicTools.getFile(prefix + "/src/index.html"));
        fileMap.put("style.css", BasicTools.getFile(prefix + "/src/style.css"));
        fileMap.put("script.js", BasicTools.getFile(prefix + "/src/script.js"));
        fileMap.put("default.html", BasicTools.getFile(prefix + "/src/default.html"));
        fileMap.put("forbid.html", BasicTools.getFile(prefix + "/src/forbid.html"));
//        fileMap.put("user.js", BasicTools.getFile("frontend/src/user.js"));


        System.out.println("start server at " + String.valueOf(localPort) + " " + version);
        HttpServer server = HttpServer.create(new InetSocketAddress(localPort), 0);


        HttpContext context = server.createContext("/");
        HttpContext context1 = server.createContext("/json/");
        context.setHandler(HttpHtmlServer::handleRequest);
        context1.setHandler(HttpHtmlServer::handleJsonRequest);
        server.start();
//        dbio.toggleDisplayStatus("test","testpass");
    }
}
