package cn.edu.bit.cs.todo.server;

import cn.edu.bit.cs.todo.basictools.BasicTools;
import cn.edu.bit.cs.todo.dbio.MariadbIO;
import cn.edu.bit.cs.todo.dbio.MiddleIO;
import cn.edu.bit.cs.todo.exceptions.LogicException;
import cn.edu.bit.cs.todo.pack.ProjectPack;
import cn.edu.bit.cs.todo.pack.RequestPack;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static cn.edu.bit.cs.todo.pack.RequestPack.createRequestPack;

public class ServerProcessor {
    public static void setPORT(int PORT) {
        ServerProcessor.PORT = PORT;
    }

    public final static String version = "v1.0.0";
    public static int PORT = 8899;
    MiddleIO middleIO;

    ServerProcessor(String user, String password, String path, int port, String databaseName) {
        middleIO = new MiddleIO(new MariadbIO(user, password, path, port, "mariadb", databaseName));
    }


    public class ServerCall implements Callable<Void> {

        private Socket clientConnection;

        public ServerCall(Socket connection) {
            this.clientConnection = connection;
        }

        RequestPack process(RequestPack requestPack) {
            RequestPack retPack = null;
            if (requestPack.isAction("verifyUser")) {
                try {
                    boolean b = middleIO.verifyUser(requestPack.getUname(), requestPack.getUpass());
                    retPack = createRequestPack(true, b);
                } catch (LogicException e) {
                    e.getMsg();
                    e.printStackTrace();
                    return createRequestPack(false, e.getMsg());
                }
            } else if (
                    requestPack.isAction("addUser")
            ) {
                try {
                    middleIO.addUser(requestPack.getUname(), requestPack.getUemail()
                            , requestPack.getUpass());
                    retPack = createRequestPack(true, "");
                } catch (LogicException e) {
//                    e.getMsg();
                    e.printStackTrace();
                    return createRequestPack(false, e.getMsg());
                }

            } else if (requestPack.isAction("modifyUser")) {
                try {
                    middleIO.modifyUser(requestPack.getUnameOld()
                            , requestPack.getUpassOld(), requestPack.getUname(),
                            requestPack.getUemail(), requestPack.getUpass());
                    retPack = createRequestPack(true, "");
                } catch (LogicException e) {
                    e.printStackTrace();
                    return createRequestPack(false, e.getMsg());
                }
            } else if (requestPack.isAction("saveAll")) {
                try {
                    middleIO.saveAllProjects(requestPack.getUname(), requestPack.getUpass(), requestPack.getJsonData());
                    retPack = createRequestPack(true, "");
                } catch (LogicException e) {
                    e.printStackTrace();
                    return createRequestPack(false, e.getMsg());
                }


            } else if (requestPack.isAction("getAll")) {
                try {
                    ArrayList<ProjectPack> allProjects = middleIO.getAllProjects(requestPack.getUname(), requestPack.getUpass());
                    retPack = new RequestPack()
                            .setSuccess(true)
                            .setJsonData(allProjects);
                } catch (LogicException e) {
                    e.printStackTrace();
                    return createRequestPack(false, e.getMsg());
                }
            } else if (requestPack.isAction("getUser")) {

                try {
                    retPack = middleIO.getUser(requestPack.getUname(),
                            requestPack.getUpass());
                    retPack.setSuccess(true);
                } catch (LogicException e) {
                    e.printStackTrace();
                    return createRequestPack(false, e.getMsg());
                }


            } else if (requestPack.isAction("getProjectsUpdateTime")) {

                try {
                    LocalDateTime projectsUpdateTime = middleIO.getProjectsUpdateTime(requestPack.getUname(),
                            requestPack.getUpass());
                    retPack = new RequestPack()
                            .setSuccess(true)
                            .setUpdateTime(projectsUpdateTime);

                } catch (LogicException e) {
                    e.printStackTrace();
                    return createRequestPack(false, e.getMsg());
                }
            } else if (requestPack.isAction("getDisplay")) {

                    try {
                        boolean displayStatus = middleIO.getDisplayStatus(requestPack.getUname());
                        retPack = new RequestPack()
                                .setSuccess(true)
                                .setDisplay(displayStatus);
                    } catch (LogicException e) {
                        e.printStackTrace();
                        return createRequestPack(false, e.getMsg());
                    }

            } else if (requestPack.isAction("toggleDisplay")){
                try {
                    middleIO.toggleDisplayStatus(requestPack.getUname(), requestPack.getUpass());
                    retPack = new RequestPack()
                            .setSuccess(true);
                } catch (LogicException e) {
                    e.printStackTrace();
                    return createRequestPack(false, e.getMsg());
                }

            }
            else if (requestPack.isAction("testTESTtest")) {
                retPack = new RequestPack()
                        .setSuccess(true)
                        .setMsg("TestResult");
            } else {
                retPack = createRequestPack(false, "wrong action!");
            }
            return retPack;
        }

        public Void call() {
            //  数据发送完毕后，自动地断开链接
            System.out.println("接收到客户端的连接: " + clientConnection);
            System.out.println("负责处理的线程: " + Thread.currentThread());

            OutputStreamWriter out = null;
            String inputString = null;
            try {

                Reader reader = new InputStreamReader(clientConnection.getInputStream(),
                        Charset.forName("utf-8"));
                BufferedReader bufferedReader = new BufferedReader(reader);

                inputString = bufferedReader.readLine();

                if (inputString != null) {
                    System.out.println(inputString);
                } else {
                    System.out.println("未能成功读取");
                }

                System.out.println("正在提取RequestPack");
                RequestPack requestPack = createRequestPack(inputString);

                System.out.println("准备发往数据库");
                RequestPack resultPack = process(requestPack);
                System.out.println("返回:" + resultPack.toJson())
                ;
                out = new OutputStreamWriter(
                        clientConnection.getOutputStream(),
                        Charset.forName("utf-8")
                );

//                Date now = new Date();
                out.write(resultPack.toJson() + "\r\n");
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    clientConnection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    void launch() {
//        middleIO.getUrl();
//        System.out.println(middleI);
        ExecutorService pool = Executors.newFixedThreadPool(50);
        try (ServerSocket server = new ServerSocket(PORT)) {
            System.out.println("正在监听端口" + PORT + ",等待客户端链接...");
            while (true) {
                Socket clientConnection = server.accept();
                Callable<Void> task = new ServerCall(clientConnection);
                pool.submit(task);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }
    }

    public static void main(String[] args) {
        System.out.println(ServerProcessor.version);

        String databaseUser = BasicTools.getEnvConfig("DATABASE_USER","root");
        String databasePassword = BasicTools.getEnvConfig("DATABASE_PASSWORD","passwordW");
        String databaseHost = BasicTools.getEnvConfig("DATABASE_HOST","localhost");
        Integer databasePort = BasicTools.getEnvConfig("DATABASE_PORT",3306);
        String databaseName = BasicTools.getEnvConfig("DATABASE_NAME","todo_pro");
        Integer serverPort =BasicTools.getEnvConfig("SERVER_PORT",8899);


//        databaseHost = "59.110.233.235";

        ServerProcessor.setPORT(serverPort);
        ServerProcessor serverProcessor = new ServerProcessor(databaseUser,
                databasePassword, databaseHost, databasePort, databaseName);

        serverProcessor.launch();
    }
}
