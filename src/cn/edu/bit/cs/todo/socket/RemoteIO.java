package cn.edu.bit.cs.todo.socket;


import cn.edu.bit.cs.todo.exceptions.LogicException;
import cn.edu.bit.cs.todo.interfaces.MiddleIOInterface;
import cn.edu.bit.cs.todo.pack.ProjectPack;
import cn.edu.bit.cs.todo.pack.RequestPack;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * RemoteIO负责与远程server进行沟通，处理上层需求并发送请求，再将收到的数据进行传递。
 * 任何错误要变成LogicException输出
 *
 * @see LogicException
 */
public class RemoteIO implements MiddleIOInterface {
    private String server;
    private int port;

    public RemoteIO(String server, int port) {
        this.server = server;
        this.port = port;
    }

    public String sendToRemote(String msg) {
        String now = null;
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress(server, port), 2000);

            // 客户端先out
            OutputStreamWriter output = new OutputStreamWriter(
                    socket.getOutputStream(), Charset.forName("utf-8")
            );

            String sendString = msg + "\r\n";

            output.write(sendString);
            output.flush();

            // 再in
            Reader reader = new InputStreamReader(socket.getInputStream(),
                    Charset.forName("utf-8"));
            BufferedReader bufferedReader = new BufferedReader(reader);

            now = bufferedReader.readLine();
//            System.out.println(bufferedReader.readLine());
            if (now != null) {
                System.out.println(now);
            } else {
                System.out.println("RemoteIO未能成功读取");
            }
            bufferedReader.close();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return now;
    }

    @Override
    public boolean verifyUser(String username, String password) throws LogicException {
        RequestPack requestPack = RequestPack.createRequestPack(username, password, "verifyUser");
        String json = sendToRemote(requestPack.toJson());
//        System.out.println(json);
        RequestPack responsePack = RequestPack.createRequestPack(json);
//        System.out.println(responsePack.toJson());
        if (responsePack == null) {
            // timeout
            return false;
        }
        if (responsePack.getSuccess()) {
            return responsePack.getRes();
        } else {
            throw new LogicException(requestPack.getMsg());
        }
    }

    /**
     * never use it or implement it
     * @param usernameOrEmail
     * @return
     * @throws LogicException
     */
    @Deprecated
    @Override
    public ArrayList<ProjectPack> getAllProjects(String usernameOrEmail) throws LogicException {
        throw new LogicException("Deprecated");
    }


    @Override
    public void addUser(String userName, String email, String password) throws LogicException {
        RequestPack requestPack = RequestPack.createRequestPack(userName, email, password, "addUser");
        String json = sendToRemote(requestPack.toJson());
        RequestPack responsePack = RequestPack.createRequestPack(json);
        if (responsePack.getSuccess()) {
            return;
        } else {
            throw new LogicException(responsePack.getMsg());
        }
    }

    @Override
    public void modifyUser(String userNameOld, String passwordOld, String userNameNew, String emailNew, String passwordNew)
            throws LogicException {
        RequestPack requestPack = new RequestPack("modifyUser")
                .setUnameOld(userNameOld)
                .setUpassOld(passwordOld)
                .setUname(userNameNew)
                .setUemail(emailNew)
                .setUpass(passwordNew);
        String json = sendToRemote(requestPack.toJson());
        RequestPack responsePack = RequestPack.createRequestPack(json);
        if (responsePack.getSuccess()) {
            return;
        } else {
            throw new LogicException(responsePack.getMsg());
        }
    }

    @Override
    public void saveAllProjects(String username, String password, ArrayList<ProjectPack> arrayList)
            throws LogicException {
        RequestPack requestPack = new RequestPack("saveAll")
                .setUname(username)
                .setUpass(password)
                .setJsonData(arrayList);
        String json = sendToRemote(requestPack.toJson());
        RequestPack responsePack = RequestPack.createRequestPack(json);
        if (responsePack.getSuccess()) {
            return;
        } else {
            throw new LogicException(responsePack.getMsg());
        }
    }

    @Override
    public ArrayList<ProjectPack> getAllProjects(String username, String password) throws LogicException {
        RequestPack requestPack = new RequestPack("getAll")
                .setUname(username)
                .setUpass(password);
        String json = sendToRemote(requestPack.toJson());
        RequestPack responsePack = RequestPack.createRequestPack(json);
        if (responsePack.getSuccess()) {
            return responsePack.getJsonData();
        } else {
            throw new LogicException(requestPack.getMsg());
        }
    }

    @Override
    public boolean getDisplayStatus(String username) throws LogicException {
        RequestPack requestPack = new RequestPack("getDisplay")
                .setUname(username);
        String json = sendToRemote(requestPack.toJson());
        RequestPack responsePack = RequestPack.createRequestPack(json);
        if (responsePack.getSuccess()) {
            return responsePack.getDisplay();
        } else {
            throw new LogicException(requestPack.getMsg());
        }
    }

    @Override
    public void toggleDisplayStatus(String username, String password) throws LogicException {
        RequestPack requestPack = new RequestPack("toggleDisplay")
                .setUname(username)
                .setUpass(password);
        String json = sendToRemote(requestPack.toJson());
        RequestPack responsePack = RequestPack.createRequestPack(json);
        if (responsePack.getSuccess()) {
            return;
        } else {
            throw new LogicException(requestPack.getMsg());
        }
    }

    @Override
    public RequestPack getUser(String username, String password) throws LogicException {
        RequestPack requestPack = new RequestPack("getUser")
                .setUname(username)
                .setUpass(password);
        String json = sendToRemote(requestPack.toJson());
        RequestPack responsePack = RequestPack.createRequestPack(json);
        if (responsePack.getSuccess()) {
            return responsePack;
        } else {
            throw new LogicException(requestPack.getMsg());
        }
    }

    @Override
    public LocalDateTime getProjectsUpdateTime(String username, String password) throws LogicException {
        RequestPack requestPack = new RequestPack("getProjectsUpdateTime")
                .setUname(username)
                .setUpass(password);
        String json = sendToRemote(requestPack.toJson());
        RequestPack responsePack = RequestPack.createRequestPack(json);
        if (responsePack.getSuccess()) {
            return responsePack.getUpdateTime();
        } else {
            throw new LogicException(responsePack.getMsg());
        }
    }


}
