package cn.edu.bit.cs.todo.dbio;

import cn.edu.bit.cs.todo.pack.ProjectPack;

import java.io.File;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

public class SQLiteIO extends DatabaseIO {

    public SQLiteIO(String user, String password, String path, String databaseType, String fileName) {
        super(user, password, path, -1, databaseType, fileName);
    }

    public static SQLiteIO createSQLiteIO(String user,String password, String path, String databaseType, String fileName){
        SQLiteIO sqLiteIO = new SQLiteIO(user,password,path,databaseType,fileName);
        File file = new File(path+'/'+fileName);
        if(file.exists()){
            System.out.println(path+'/'+fileName+"已存在");

        }else {
            System.out.println(path+'/'+fileName+"不存在");
            sqLiteIO.createTable();
        }
        return sqLiteIO;
    }
    /**
     * 若不存在就自动建立数据库表，在初始化的时候调用
     */
    void createTable() {
        Connection connection = null;
        String url = getUrl();
        ArrayList<String> sqlArrayList = new ArrayList<>();
        sqlArrayList.add("PRAGMA foreign_keys = ON");
        sqlArrayList.add("CREATE TABLE IF NOT EXISTS `users` (\n" +
                "  `uid` INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  `username` TEXT NOT NULL UNIQUE,\n" +
                "  `email` TEXT NOT NULL UNIQUE,\n" +
                "  `password` TEXT NOT NULL,\n" +
                "  `updateTime` INTEGER DEFAULT (datetime('now','localtime')),\n" +
                "  `projectsUpdateTime` INTEGER DEFAULT (datetime('now','localtime')),\n" +
                "  `display` INTEGER DEFAULT 0" +
                ");");
        sqlArrayList.add("CREATE TABLE IF NOT EXISTS `projects` (\n" +
                "  `id` INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "  `uid` INT NULL,\n" +
                "  `title` TEXT NOT NULL,\n" +
                "  `project_id` TEXT NULL,\n" +
                "  `json` TEXT NULL,\n" +
                "  `updateTime` INTEGER DEFAULT (datetime('now','localtime')),\n" +
                "    FOREIGN KEY (`uid`)\n" +
                "    REFERENCES `users` (`uid`)\n" +
                "    ON DELETE NO ACTION\n" +
                "    ON UPDATE NO ACTION\n" +
                ");");

        sqlArrayList.add("CREATE  TRIGGER insert_users AFTER INSERT \n" +
                "ON users\n" +
                "BEGIN\n" +
                "update users set updateTime = (datetime('now','localtime')) WHERE users.uid = new.uid;\n" +
                "END;");
        sqlArrayList.add("CREATE  TRIGGER update_users AFTER UPDATE \n" +
                "ON users\n" +
                "BEGIN\n" +
                "update users set updateTime = (datetime('now','localtime')) WHERE users.uid = new.uid;\n" +
                "END;\n");
        sqlArrayList.add("CREATE  TRIGGER delete_users AFTER DELETE \n" +
                "ON users\n" +
                "BEGIN\n" +
                "update users set updateTime = (datetime('now','localtime')) WHERE users.uid = old.uid;\n" +
                "END;");
        sqlArrayList.add("CREATE  TRIGGER insert_projects AFTER INSERT \n" +
                "ON projects\n" +
                "BEGIN\n" +
                "update users set projectsUpdateTime = (datetime('now','localtime')) WHERE users.uid = new.uid;\n" +
                "END;");
        sqlArrayList.add("CREATE  TRIGGER update_projects AFTER UPDATE \n" +
                "ON projects\n" +
                "BEGIN\n" +
                "update users set projectsUpdateTime = (datetime('now','localtime')) WHERE users.uid = new.uid;\n" +
                "END;");
        sqlArrayList.add("CREATE  TRIGGER delete_projects AFTER DELETE \n" +
                "ON projects\n" +
                "BEGIN\n" +
                "update users set projectsUpdateTime = (datetime('now','localtime')) WHERE users.uid = old.uid;\n" +
                "END;");
        try {
            // 创建数据库连接
            connection = getConnection();
            // 准备预处理数据库命令
            Statement statement = connection.createStatement();

            for (String sql : sqlArrayList) {
                statement.addBatch(sql);
            }

            statement.executeBatch();

            System.out.println("table Create");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try { // 确保各种数据库对象都被关闭

                if (null != connection) {
                    connection.close();
                }
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    @Override
    LocalDateTime timeStampStringToLocalDateTime(String timestamp) {
        int[] times = Arrays.stream(timestamp.split("-| |:")).mapToInt(s -> Integer.parseInt(s)).toArray();
        return LocalDateTime.of(times[0], times[1], times[2], times[3], times[4], times[5]);
    }

    @Override
    String getUrl() {
        return "jdbc:" + databaseType + ":" + path + "/" + databaseName;
    }

}
