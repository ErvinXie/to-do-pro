package cn.edu.bit.cs.todo.dbio;

import cn.edu.bit.cs.todo.exceptions.LogicException;
import cn.edu.bit.cs.todo.pack.ProjectPack;
import cn.edu.bit.cs.todo.pack.RequestPack;
import org.mariadb.jdbc.*;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;


/**
 * 使用JDBC管理底层数据库IO
 *
 * @author Kaiyu Feng
 * @version 1.0.0
 */
public abstract class DatabaseIO {

    protected static String userTableName = "users";
    protected static String projectTableName = "projects";

    protected String user;
    protected String password;
    protected String databaseName;
    protected String port;
    protected String path;
    protected String databaseType;
    protected String url;

    /**
     * @param timestamp 时间戳"2018-11-10 12:12:40"
     * @return LocalDateTime类型
     */
    abstract LocalDateTime timeStampStringToLocalDateTime(String timestamp);

    /**
     * @param user         数据库用户名，SQLITE不需要
     * @param password     database password
     * @param path         database hostname, path is for SQLite
     * @param port         database port
     * @param databaseType mariadb or SQLite
     * @param databaseName todo_pro by default
     */
    public DatabaseIO(String user, String password, String path, int port, String databaseType, String databaseName) {
        this.user = user;
        this.databaseName = databaseName;
        this.password = password;
        this.port = String.valueOf(port);
        this.path = path;
        this.databaseType = databaseType;
        this.url = getUrl();
    }

    public DatabaseIO(String user, String password, String path, String databaseName) {
        this(user, password, path, 3306, "mariadb", databaseName);
    }

    public DatabaseIO(String user, String password, String path, int port) {
        this(user, password, path, port, "mariadb", "todo_pro");
    }

    abstract String getUrl();

    protected synchronized Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, this.user, this.password);

    }

    /**
     * 测试数据库连接和数据的查询
     *
     * @deprecated 将在未来删除
     */
    public void testQuery() {
        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;
        String url = getUrl();
        try {
            // 创建数据库连接
            connection = getConnection();

            // 准备创建操作与存取的数据库命令
            statement = connection.createStatement();
            // 查询表中保存的数据
            rs = statement.executeQuery("select * from " + userTableName);
            // 循环输出查询到的数据
            while (rs.next()) {
                System.out.println("------------------");
                System.out.println("Username = " + rs.getString("username"));
                System.out.println("Email = " + rs.getString("email"));
                System.out.println("Password =" + rs.getString("password"));
//                System.out.println(rs.getInt("updateTime"));
                System.out.println("Date =" + rs.getString("updateTime"));
            }

            // 准备创建操作与存取的数据库命令
            statement = connection.createStatement();
            // 查询表中保存的数据
            rs = statement.executeQuery("select * from " + projectTableName);
            // 循环输出查询到的数据
            while (rs.next()) {
                System.out.println("------------------");
                System.out.println("Title = " + rs.getString("title"));
                System.out.println("ProjectId = " + rs.getString("project_id"));
                System.out.println("Json =" + rs.getString("json"));
//                System.out.println(rs.getInt("updateTime"));
                System.out.println("UpdateTime =" + rs.getString("updateTime"));
            }
        } catch (SQLException e) {
            System.out.println("hhh");
            System.err.println(e.getMessage());
            System.err.println(e.getSQLState());
            System.err.println(e.getErrorCode());
        } finally {
            try { // 确保各种数据库对象都被关闭
                if (connection != null)
                    connection.close();
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.err.println(e);
                System.err.println(e.getSQLState());
                System.err.println(e.getErrorCode());
                System.out.println("sdf");
            }
        }
    }


    /**
     * 作为Display属性伴生品，不要被其他函数调用，除非你知道自己在做什么
     * @param nameOrEmail 用户名或邮箱
     * @return userID
     * @throws SQLException IO exception
     */
    public int selectUser(String nameOrEmail) throws SQLException {
        Connection connection = null;
        ResultSet rs = null;
        String url = getUrl();
        String sql = "SELECT uid FROM " + userTableName + " WHERE (username = ? or email = ?)";
        try {
            // 创建数据库连接
            connection = getConnection();

            // 准备预处理数据库命令
            PreparedStatement stmt = connection.prepareStatement(sql);

            // set prepared params
            stmt.setString(1, nameOrEmail);
            stmt.setString(2, nameOrEmail);
            rs = stmt.executeQuery();

            if (rs.next()) {
                System.out.println("------------------");
                int retValue = rs.getInt("uid");
                System.out.println("ClientID = " + retValue);
                // return uid
                return retValue;
            }
            int rowCount = rs.getRow();
            if (rowCount == 0) {
                // get nothing
                return 0;
            }
        } finally {
            try { // 确保各种数据库对象都被关闭
                if (connection != null) {
                    connection.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        return 0;
    }


    // 获得用户唯一uid
    public int selectUser(String nameOrEmail, String password) throws SQLException {
        Connection connection = null;
        ResultSet rs = null;
        String url = getUrl();
        String sql = "SELECT uid FROM " + userTableName + " WHERE (username = ? or email = ?) and password = ?";
        try {
            // 创建数据库连接
            connection = getConnection();

            // 准备预处理数据库命令
            PreparedStatement stmt = connection.prepareStatement(sql);

            // set prepared params
            stmt.setString(1, nameOrEmail);
            stmt.setString(2, nameOrEmail);
            stmt.setString(3, password);

            rs = stmt.executeQuery();

            if (rs.next()) {
                System.out.println("------------------");
                int retValue = rs.getInt("uid");
                System.out.println("ClientID = " + retValue);
                // return uid
                return retValue;
            }
            int rowCount = rs.getRow();
            if (rowCount == 0) {
                // get nothing
                return 0;
            }
        } finally {
            try { // 确保各种数据库对象都被关闭
                if (connection != null) {
                    connection.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.err.println(e);
            }
        }

        return 0;
    }

    public LocalDateTime getProjectsUpdateTime(int uid) throws SQLException {
        Connection connection = null;
        ResultSet rs = null;
        String url = getUrl();
        String retValue = null;
        String sql = "SELECT projectsUpdateTime FROM " + userTableName + " WHERE uid = ?";
        try {
            // 创建数据库连接
            connection = getConnection();

            // 准备预处理数据库命令
            PreparedStatement stmt = connection.prepareStatement(sql);

            // set prepared params
            stmt.setInt(1, uid);
            rs = stmt.executeQuery();

            if (rs.next()) {
                retValue = rs.getString("projectsUpdateTime");
            }

        } finally {
            try { // 确保各种数据库对象都被关闭
                if (connection != null) {
                    connection.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        return timeStampStringToLocalDateTime(retValue);
    }


    // 查询是否合法
    public boolean verifyUser(String username, String password) throws SQLException {
        return this.selectUser(username, password) != 0;
    }

    public boolean haveUser(String username, String email) throws SQLException {
        Connection connection = null;
        ResultSet rs = null;
        String url = getUrl();
        String sql = "SELECT uid FROM " + userTableName + " WHERE username = ? OR Email = ?";
        try {
            // 创建数据库连接
            connection = getConnection();

            // 准备预处理数据库命令
            PreparedStatement stmt = connection.prepareStatement(sql);

            // set prepared params
            stmt.setString(1, username);
            stmt.setString(2, email);

            rs = stmt.executeQuery();

            // 循环输出查询到的数据
            return rs.next();

        } finally {
            try { // 确保各种数据库对象都被关闭
                if (connection != null) {
                    connection.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    // 测试数据库连接和数据的查询
    public void addUser(String username, String email, String password) throws SQLException {
        Connection connection = null;
        String url = getUrl();
        String sql = "INSERT INTO " + userTableName + " (username, email, password) VALUES(?,?,?)";
        try {
            // 创建数据库连接
            connection = getConnection();
            // 准备预处理数据库命令
            PreparedStatement stmt = connection.prepareStatement(sql);

            // set prepared params
            stmt.setString(1, username);
            stmt.setString(2, email);
            stmt.setString(3, password);

            int updateResult = stmt.executeUpdate();

            System.out.println(updateResult + " Inserted");

        } finally {
            try { // 确保各种数据库对象都被关闭

                if (null != connection) {
                    connection.close();
                }
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    /**
     * 获得用户全部信息
     *
     * @param uid userId
     */
    public RequestPack getUser(int uid) throws SQLException, LogicException {
        Connection connection = null;
        ResultSet rs = null;
        String url = getUrl();
        String sql = "SELECT username,email,password,updateTime,display FROM " + userTableName + " WHERE uid = ?";
        RequestPack ret = null;
        try {
            // 创建数据库连接
            connection = getConnection();

            // 准备预处理数据库命令
            PreparedStatement stmt = connection.prepareStatement(sql);

            // set prepared params
            stmt.setInt(1, uid);
            rs = stmt.executeQuery();

            // 循环输出查询到的数据
            System.out.println("------------------");
            if (rs.next()) {
                String username = rs.getString("username");
                String email = rs.getString("email");
                String password = rs.getString("password");
                String updateTime = rs.getString("updateTime");
                boolean display = rs.getBoolean("display");

                ret = new RequestPack().setUname(username)
                        .setUemail(email)
                        .setUpass(password)
                        .setUpdateTime(timeStampStringToLocalDateTime(updateTime))
                        .setDisplay(display);
                return ret;
            } else {
                // 如果无uid存在
                throw new LogicException("no such uid");
            }


        } finally {

            try { // 确保各种数据库对象都被关闭
                if (connection != null) {
                    connection.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    public void updateUser(int uid, String userName, String email, String password) throws SQLException {
        Connection connection = null;
        ResultSet rs = null;
        String url = getUrl();
        String sql = "UPDATE " + userTableName + " SET username = ?, email = ?, password = ?" + " WHERE uid = ?";

        try {
            // 创建数据库连接
            connection = getConnection();
            // 准备预处理数据库命令
            PreparedStatement stmt = connection.prepareStatement(sql);


            stmt.setString(1, userName);
            stmt.setString(2, email);
            stmt.setString(3, password);
            stmt.setInt(4, uid);

            stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try { // 确保各种数据库对象都被关闭
                if (connection != null) {
                    connection.setAutoCommit(true);
                    connection.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    public ArrayList<ProjectPack> getAllProject(int uid) throws SQLException {
        ArrayList<ProjectPack> array = new ArrayList<>();
        Connection connection = null;
        ResultSet rs = null;
        String url = getUrl();
        String sql = "SELECT title,project_id,json,updateTime FROM " + projectTableName + " WHERE uid = ?";
        try {
            // 创建数据库连接
            connection = getConnection();

            // 准备预处理数据库命令
            PreparedStatement stmt = connection.prepareStatement(sql);

            // set prepared params
            stmt.setInt(1, uid);
            rs = stmt.executeQuery();

            // 循环输出查询到的数据
            while (rs.next()) {
                System.out.println("------------------");
                String title = rs.getString("title");
                String project_id = rs.getString("project_id");
                String json = rs.getString("json");
                String updateTime = rs.getString("updateTime");
                array.add(ProjectPack.createProjectPack(title, project_id, json, timeStampStringToLocalDateTime(updateTime)));
            }
        } finally {
            try { // 确保各种数据库对象都被关闭
                if (connection != null) {
                    connection.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
        return array;
    }


    public void replaceAllProject(int uid, ArrayList<ProjectPack> array) {
        Connection connection = null;
        ResultSet rs = null;
        String url = getUrl();
        String sqlDelete = "DELETE FROM " + projectTableName + " WHERE uid = ?";
        String sqlInsert = "INSERT INTO " + projectTableName + "(uid,title, project_id, json) VALUES(?,?,?,?)";

        try {
            // 创建数据库连接
            connection = getConnection();
            connection.setAutoCommit(false);
            // 准备预处理数据库命令
            PreparedStatement deleteStmt = connection.prepareStatement(sqlDelete);

            // set prepared params
            deleteStmt.setInt(1, uid);

            deleteStmt.executeUpdate();
            PreparedStatement insertStmt = connection.prepareStatement(sqlInsert);
            for (ProjectPack pack : array) {
                insertStmt.setInt(1, uid);
                insertStmt.setString(2, pack.getTitle());
                insertStmt.setString(3, pack.getProjectId());
                insertStmt.setString(4, pack.getData());
                insertStmt.addBatch();
            }

            insertStmt.executeBatch();
            connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try { // 确保各种数据库对象都被关闭
                if (connection != null) {
                    connection.setAutoCommit(true);
                    connection.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    public void toggleDisplayStatus(int uid) {
        Connection connection = null;
        ResultSet rs = null;
        String url = getUrl();
        String sql = "UPDATE " + userTableName + " SET display = display <> 1" + " WHERE uid = ?";

        try {
            // 创建数据库连接
            connection = getConnection();
            // 准备预处理数据库命令
            PreparedStatement stmt = connection.prepareStatement(sql);

            stmt.setInt(1, uid);

            stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try { // 确保各种数据库对象都被关闭
                if (connection != null) {
                    connection.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    public boolean getDisplayStatus(int uid) throws SQLException, LogicException {
        Connection connection = null;
        ResultSet rs = null;
        String url = getUrl();
        String sql = "SELECT display FROM " + userTableName + " WHERE uid = ?";
        RequestPack ret = null;
        try {
            // 创建数据库连接
            connection = getConnection();
            PreparedStatement stmt = connection.prepareStatement(sql);

            stmt.setInt(1, uid);
            rs = stmt.executeQuery();

            if (rs.next()) {
                boolean display = rs.getBoolean("display");
                return display;
            } else {
                // 如果无uid存在
                throw new LogicException("no such uid");
            }

        } finally {

            try {
                if (connection != null) {
                    connection.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }
}
