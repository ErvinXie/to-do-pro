package cn.edu.bit.cs.todo.dbio;

import cn.edu.bit.cs.todo.exceptions.LogicException;
import cn.edu.bit.cs.todo.pack.ProjectPack;
import cn.edu.bit.cs.todo.pack.RequestPack;

import java.sql.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimeZone;

public class MariadbIO extends DatabaseIO {

    public MariadbIO(String user, String password, String path, int port, String databaseType, String databaseName) {
        super(user, password, path, port, databaseType, databaseName);
    }

    @Override
    String getUrl() {
        return "jdbc:" + databaseType + "://" + path + ":" + port + "/" + databaseName;
    }
    LocalDateTime timeStampStringToLocalDateTime(String timestamp){
        int[] times = Arrays.stream(timestamp.split("-| |:|\\.")).mapToInt(s -> Integer.parseInt(s)).toArray();
        return LocalDateTime.of(times[0],times[1],times[2],times[3],times[4],times[5],times[6]);
    }




    public static void main(String[] args) {
        try {
            MariadbIO dbio = new MariadbIO("root", "passwordW", "59.110.233.235", 3306, "mariadb", "todo_pro");
            dbio.selectUser("test", "test");
            System.out.println("sdf " + dbio.haveUser("test",""));
//            dbio.addUser("fk2", "fls", "ossspp");
            ArrayList<ProjectPack> arrayList = new ArrayList<>();
            arrayList.add(ProjectPack.createProjectPack("aadfga", "sdfsdf", "data+a"));
            arrayList.add(ProjectPack.createProjectPack("bdddgggbb", "sdfsdddddf", "data+ea"));
            arrayList.add(ProjectPack.createProjectPack("ccdfgdfg", "ssss", "data+ad"));

            dbio.replaceAllProject(1, arrayList);
            System.out.println(dbio.getAllProject(1));
            dbio.testQuery();
        } catch (SQLSyntaxErrorException e) {
            System.out.println("Syntax ERROR");
        } catch (SQLIntegrityConstraintViolationException e) {
            System.out.println("Duplicate");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
