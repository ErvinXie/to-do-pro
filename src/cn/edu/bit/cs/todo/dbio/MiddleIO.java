package cn.edu.bit.cs.todo.dbio;

import cn.edu.bit.cs.todo.exceptions.LogicException;
import cn.edu.bit.cs.todo.interfaces.MiddleIOInterface;
import cn.edu.bit.cs.todo.pack.ProjectPack;
import cn.edu.bit.cs.todo.pack.RequestPack;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MiddleIO implements MiddleIOInterface {
    private DatabaseIO dbio;

    public MiddleIO() {
    }

    public MiddleIO(DatabaseIO dbio) {
        this.dbio = dbio;
    }

    @Override
    public boolean verifyUser(String username, String password) throws LogicException {
        try {
            return dbio.verifyUser(username, password);
        } catch (SQLException e) {
            throw new LogicException("数据库操作异常,无法查验用户");
        } catch (Exception e) {
            throw new LogicException("添加用户失败");
        }
    }

    /**
     * 这个方法用于前端进行Display
     * 尽管不需要传入password，但是会检查display 是否为 true
     *
     * @param usernameOrEmail 账户
     * @return 所有projects
     */
    @Override
    public ArrayList<ProjectPack> getAllProjects(String usernameOrEmail) throws LogicException {
        int userId;
        try {
            userId = dbio.selectUser(usernameOrEmail);
            if (userId == 0) {
                throw new LogicException("用户名或密码错误");
            }
            boolean displayStatus = dbio.getDisplayStatus(userId);
            if (displayStatus){
                return dbio.getAllProject(userId);
            }else{
                throw new LogicException("没有展示权限");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new LogicException("数据库操作异常");
        }

    }

    @Override
    public void addUser(String userName, String email, String password) throws LogicException {
        boolean haveUser;
        try {
            haveUser = dbio.haveUser(userName, email);
            if("" == userName){
                throw new LogicException("用户名不能为空");
            }
            if(""== email){
                throw new LogicException("邮箱不能为空");
            }else{
                String regEx1 = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
                Pattern p;
                Matcher m;
                p = Pattern.compile(regEx1);
                m = p.matcher(email);
                if (!m.matches())
                    throw new LogicException("邮箱不合法");
            }
            if(password == ""){
                throw new LogicException("密码不能为空");
            }

            if (haveUser) {
                throw new LogicException("已经存在用户,无法添加");
            } else {
                dbio.addUser(userName, email, password);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new LogicException("数据库操作异常,无法添加用户");
        }

    }

    @Override
    public RequestPack getUser(String username, String password) throws LogicException {
        int uid;
        RequestPack requestPack = null;
        try {
            uid = dbio.selectUser(username, password);

            if (uid == 0) {
                throw new LogicException("没有这个用户");
            } else {
                requestPack = dbio.getUser(uid);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return requestPack;
    }

    @Override
    public void modifyUser(String userNameOld, String passwordOld, String userNameNew, String emailNew, String passwordNew) throws LogicException {
        int uid;
        try {
            uid = dbio.selectUser(userNameOld, passwordOld);
            if (uid == 0) {
                throw new LogicException("没有这个用户");
            } else {
                dbio.updateUser(uid, userNameNew, emailNew, passwordNew);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new LogicException("数据库运行错误");
        }
    }

    @Override
    public void saveAllProjects(String username, String password, ArrayList<ProjectPack> arrayList) throws LogicException {
        try {
            int userId = dbio.selectUser(username, password);
            if (userId == 0) {
                throw new LogicException("用户名或密码错误");
            } else {
                dbio.replaceAllProject(userId, arrayList);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new LogicException("数据库操作异常");
        }
    }

    @Override
    public LocalDateTime getProjectsUpdateTime(String username, String password) throws LogicException {
        int uid;
        LocalDateTime localDateTime = null;
        try {
            uid = dbio.selectUser(username, password);
            if (uid == 0) {
                throw new LogicException("用户名或密码错误");
            }
            localDateTime = dbio.getProjectsUpdateTime(uid);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return localDateTime;
    }

    @Override
    public ArrayList<ProjectPack> getAllProjects(String username, String password) throws LogicException {
        try {
            int userId = dbio.selectUser(username, password);
            if (userId == 0) {
                throw new LogicException("用户名或密码错误");
            } else {
                return dbio.getAllProject(userId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new LogicException("数据库操作异常");
        }
    }


    @Override
    public boolean getDisplayStatus(String username) throws LogicException {
        int userId;
        try {
            userId = dbio.selectUser(username);
            if (userId == 0) {
                throw new LogicException("用户名或密码错误");
            } else {
                return dbio.getDisplayStatus(userId);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new LogicException("数据库操作异常");
        }
    }


    @Override
    public void toggleDisplayStatus(String username, String password) throws LogicException {
        int userId;
        try {
            userId = dbio.selectUser(username, password);
            if (userId == 0) {
                throw new LogicException("用户名或密码错误");
            } else {
                dbio.toggleDisplayStatus(userId);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new LogicException("数据库操作异常");
        }

    }

}
