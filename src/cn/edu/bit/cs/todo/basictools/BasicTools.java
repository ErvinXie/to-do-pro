package cn.edu.bit.cs.todo.basictools;

import java.io.*;

public class BasicTools {
    public static String getEnvConfig(String envName,String defaultValue) {
        String param = null;
        param = System.getenv(envName);
        if(null != param){
            return param;
        }
        return defaultValue;
    }

    public static int getEnvConfig(String envName,int defaultValue) {
        String paramString = null;
        paramString = System.getenv(envName);
        if(null != paramString){
            int param = Integer.parseInt(paramString);
            return param;
        }
        return defaultValue;
    }
    public static String getFile(String path) throws FileNotFoundException {
        File file = new File(path);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String ret = "";
        String cur = "";
        while(true){
            try {
                if (!((cur = bufferedReader.readLine())!=null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            ret+=cur+"\n";
        }
        return ret;
    }

    public static void main(String[] args) throws FileNotFoundException {
        String file = BasicTools.getFile("/home/fky/code/git/mine/to-do-pro/src/cn/edu/bit/cs/todo/basictools/BasicTools.java");
        System.out.println(file);
    }
}
