package cn.edu.bit.cs.todo.model;

import cn.edu.bit.cs.todo.controller.myEventView;

import java.util.Date;

public class Event extends AbstractEvent {
    private Date startTime = new Date();
    private Date endTime = new Date();
    private int priority = 6;




    //View...
    transient myEventView eventView = null;

    public Event () {
        super();
        System.out.println("Event ["+this.getTitle()+"] created");
//        initializeView();
    }
    public Event (String title) {
        super(title);
        System.out.println("Event "+"["+title+"]"+" created");
//        initializeView();
    }
    public Event (String title , String remarks) {
        super(title,remarks);
        System.out.println("Event "+"["+title+"]"+" remarked with "+remarks+" created");
//        initializeView();
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public int getPriority() {
        return priority;
    }

    public Boolean setStartTime(Date startTime) {
        this.startTime = startTime;

        return true;
    }

    public Boolean setEndTime(Date endTime) {
        this.endTime = endTime;

        return true;
    }

    public Boolean setPriority(int priority) {
        this.priority = priority;

        return true;
    }



    @Override
    public double getRightX() {
//        System.out.println("Event["+this.getTitle()+"]: "+eventView.getLayoutX()+eventView.getWidth());
        return father.subEventListView.getLayoutX()+father.subEventListView.getWidth();
    }

    void initializeView(){
        this.eventView = new myEventView(this);
    }

    public myEventView getEventView() {
        if(eventView==null){
            initializeView();
        }
        return eventView;
    }
    public void show(){
        if(eventView == null){
            initializeView();
        }

    }

    public void showList(){

        if(father!=null) {
            for (int i = 0; i < father.subEventList.size(); i++) {
                if (father.subEventList.get(i).isShowingList == true) {
                    father.subEventList.get(i).hideList();
                }
                father.subEventList.get(i).listToBeShown = false;
            }
        }

        if(subEventListView == null){
            initializeListView();
        }

        this.subEventListView.refresh();
        holder.getChildren().add(this.subEventListView);

//        father.subEventListView.foldBackwards(father.getDepth());

        for(int i=0;i<subEventList.size();i++){
            if(subEventList.get(i).listToBeShown ==true){
                subEventList.get(i).showList();
            }
        }
        listToBeShown = true;
        isShowingList = true;
    }

    @Override
    public Boolean delete() {
        this.to_be_deleted = true;
        this.listToBeShown = false;
        for(int i = subEventList.size()-1;i>=0;i--){
            subEventList.get(i).delete();
        }

        hideList();

        for (int i = father.subEventList.size() - 1; i >= 0; i--) {
            if (father.subEventList.get(i).to_be_deleted == true) {
                father.subEventList.remove(i);
            }
//            System.out.println(father.toString());
            try {
                System.out.println(father.subEventListView.toString());
                father.subEventListView.refresh();
            }catch (Exception e){

            }
        }

        return null;
    }


}
