package cn.edu.bit.cs.todo.model;


import cn.edu.bit.cs.todo.controller.MainWindowController;
import cn.edu.bit.cs.todo.controller.myProjectView;
import cn.edu.bit.cs.todo.interfaces.Serialize;
import com.google.gson.Gson;

public class Project extends AbstractEvent implements Serialize {

    public transient MainWindowController mainWindowController = null;



    transient myProjectView projectView = null;



    public Project (){
        super();
        System.out.println("Project ["+this.getTitle()+"] created");
//        initializeView();
    }
    public Project(String title){
        super(title);
        System.out.println("Project ["+this.getTitle()+"] created");
//        initializeView();
    }
    public Project(String title, String remarks){
        super(title,remarks);
        System.out.println("Project "+"["+title+"]"+" remarked with "+remarks+" created");
//        initializeView();
    }


    void initializeView(){
        this.projectView = new myProjectView(this);
    }

    public myProjectView getProjectView() {
        if(projectView == null){
            initializeView();
        }
        return projectView;
    }
    @Override
    public void showList() {

        for (int i = 0; i < mainWindowController.projectArrayList.size(); i++) {
            if (mainWindowController.projectArrayList.get(i).isShowingList == true) {
                mainWindowController.projectArrayList.get(i).hideList();
            }
            mainWindowController.projectArrayList.get(i).listToBeShown = false;
        }


        if(subEventListView == null){
            initializeListView();
        }

        this.subEventListView.refresh();
        holder.getChildren().add(this.subEventListView);
        for(int i=0;i<subEventList.size();i++){
            if(subEventList.get(i).listToBeShown ==true){
                subEventList.get(i).showList();
            }
        }
        listToBeShown = true;
        isShowingList = true;
    }
    @Override
    public double getRightX() {
//        System.out.println("Proj["+this.getTitle()+"]: "+projectView.getLayoutX()+projectView.getWidth());
        return mainWindowController.projectListView.getLayoutX()+mainWindowController.projectListView.getWidth();
    }

    @Override
    public Boolean delete(){

        for(int i = subEventList.size()-1;i>=0;i--){
            subEventList.get(i).delete();
        }
        mainWindowController.projectArrayList.remove(this);
        mainWindowController.projectListView.refresh();
        return true;
    }



    /**
     * 主要是为了Project 的 title在修改以后依然能够做出区分，
     * 不过第一版的细粒度并没有达到，所以暂时不会使用这个属性
     * @return 一个独立的ProjectsId
     */
    @Override
    public String getProjectId() {
        return "not now";
        // in next version
    }
    @Override
    public String toJson() {
        return new Gson().toJson(this);
    }
    /**
     * 显示地表明已经实现getTitle方法
     * @return title
     */
    @Override
    public String getTitle() {
        return super.getTitle();
    }
}

