

package cn.edu.bit.cs.todo.model;

import cn.edu.bit.cs.todo.controller.myEventListView;
import cn.edu.bit.cs.todo.utils.Constants;
import javafx.scene.layout.AnchorPane;

import java.util.ArrayList;
import java.util.Date;

public abstract class AbstractEvent {
    private Date createTime;
    private String title = "";
    private String remarks = "";
    private Boolean isCompleted = false;
    public Boolean to_be_deleted = false;

    public ArrayList<Event> subEventList;
    public transient AbstractEvent father = null;

    public transient AnchorPane holder;
    public transient myEventListView subEventListView = null;


    public void setTransient(){
        for(Event i:subEventList){
            i.father=this;
            i.holder = this.holder;
            i.listToBeShown = false;
            i.setTransient();
        }
    }



    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    private int depth = 0;

    public AbstractEvent(){
        this.subEventList = new ArrayList();
        noNameCount=noNameCount+1;
        this.setTitle("Untitled"+noNameCount);
        this.setRemarks("Write Something Here...");
        this.createTime = new Date();
    }
    public AbstractEvent(String title){
        this.subEventList = new ArrayList();
        this.createTime = new Date();
        this.setRemarks("Write Something Here...");
        this.title = title;
    }
    public AbstractEvent(String title, String remarks){
        this.subEventList = new ArrayList();
        this.createTime = new Date();
        this.title = title;
        this.remarks = remarks;
    }



    static int noNameCount = 0;
    public Date getCreateTime(){
        return this.createTime;
    }

    public String getTitle() {
        return this.title;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public Boolean isCompleted() {
        return this.isCompleted;
    }

    public Boolean setTitle(String title){
        this.title = title;
        return true;
    }

    public Boolean setCreateTime(Date createTime) {
        this.createTime = createTime;
        return true;
    }

    public Boolean setRemarks(String remarks) {
        this.remarks = remarks;
        return true;
    }



    public Boolean addSubEvent(Event SubEvent){
        SubEvent.father = this;
        SubEvent.holder = this.holder;
        this.subEventList.add(SubEvent);
        SubEvent.setDepth(this.depth+1);
        if(subEventListView==null){
            initializeListView();
        }
        this.subEventListView.refresh();

        System.out.println("Event ["+SubEvent.getTitle()+"] added to sons of ["+this.getTitle()+"]");
        return true;
    }

    public Boolean addSubEvent(Event SubEvent, int x) {
        SubEvent.father = this;

        SubEvent.holder = this.holder;
        SubEvent.setDepth(this.depth+1);
        if(x<this.subEventList.size()){
            this.subEventList.add(x,SubEvent);
            this.subEventListView.refresh();
            return true;
        } else {
            return false;
        }
    }


    public abstract double getRightX();


    boolean isShowingList = false;
    boolean listToBeShown = false;

    public abstract void showList();


    public void hideList(){
        if(subEventListView==null){
            initializeListView();
        }
        isShowingList = false;
        for(int i=0;i<subEventList.size();i++){
            if(subEventList.get(i).isShowingList==true) {
                subEventList.get(i).hideList();
            }
        }
//        this.subEventListView.hide();

        holder.getChildren().remove(this.subEventListView);
        isShowingList = false;
        System.out.println("Hide ["+this.getTitle()+"]'s List");
    }

    public void initializeListView() {
        this.subEventListView = new myEventListView(this);
    }



    public Boolean complete(){
        System.out.println("["+getTitle()+"] completed");
        this.isCompleted = true;
        return true;
    }

    public Boolean deComplete() {
        System.out.println("["+getTitle()+"] decompleted");
        this.isCompleted = false;
        return true;
    }

    public abstract Boolean delete();



    //以下为调试函数

    public void printTitle() {
        System.out.println("Title: " + this.title);
    }

    public void printTime() {
        System.out.println("Created at: " + this.createTime.toString());
    }

    public void printRemarks() {
        System.out.println("Remarked with: " + this.remarks);
    }

    public void printSubEvents() {
        int index = 0;
        System.out.println("SubEvents: ");
        for (Event SubEvent : this.subEventList) {
            index += 1;
            System.out.print("SubEvent(" + index + "/" + this.subEventList.size() + "): ");
            SubEvent.printTitle();
        }
    }

    public void printInfomation(){
        System.out.println("-----------------------------------------");
        this.printTitle();
        this.printTime();
        this.printRemarks();
        this.printSubEvents();
        System.out.println("-----------------------------------------");
    }

}


