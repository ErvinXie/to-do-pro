package cn.edu.bit.cs.todo.utils;


final public class Constants {


    public static final double divideRatio = 0.312;
    public static final double topBarHeight = 30.0;
    public static final double loginBarHeight = 30.0;


    public static final double defaultX = 500;
    public static final double defaultY = 0;
    public static final double defaultWidth = 800;
    public static final double defaultHeight = 800;



    public static final double projectBarWidth = 300;
    public static final double projectBarHeight = 300;

    public static final double eventBarHeight = 60;
    public static final double eventBarWidth = 500;

    public static final double foldedEventBarWidth = 150;
    public static final double eventBarButtonHeight = 30;


    public static final double detailsVgap = 7.5;
    public static final double detailsBorderWitdh = 5.0;

    public static final double slideBarWidth = 20;

    public static final double barMoveTime = 500;
    public static final double slideBarBounceTime = 100;
    public static final double listHideTime = 200;
    public static final int animationStep = 5;

    public static final double buttonHeight = 50.0;

}
