package cn.edu.bit.cs.todo.utils;

import javafx.scene.paint.Color;

public class ColorGenerator {
    public static Color RandomColor(){
        return Color.color(Math.random(),Math.random(),Math.random());
    }
    public static Color RandomGreyColor(){
        double R = Math.random();
        return Color.color(R,R,R);
    }
    public static Color RandomGreyColor(double black, double white){
        double R = Math.random()*(white-black)+black;
        return Color.color(R,R,R);
    }
    public static double rand(double a,double b){
        return a+Math.random()*(b-a);
    }

    public static Color getGrey(double b){
        return Color.color(b,b,b);
    }


    public static Color RandomLightColor(){
        return Color.color(rand(0.8,1),rand(0.8,1),rand(0.8,1));
    }
    public static Color RandomDarkColor(){
        return Color.color(rand(0.2,0.4),rand(0.2,0.4),rand(0.2,0.4));
    }
    public static Color RandomMiddleColor(){
        return Color.color(rand(0.5,0.7),rand(0.5,0.7),rand(0.5,0.7));
    }
    public static Color RandomMiddleDarkColor(){
        return Color.color(rand(0.4,0.6),rand(0.4,0.6),rand(0.4,0.6));
    }

    public static Color TopBarColor(){
        return Color.color(1,1,1);
    }

}
