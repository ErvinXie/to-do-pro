package cn.edu.bit.cs.todo.socket;

import cn.edu.bit.cs.todo.exceptions.LogicException;
import cn.edu.bit.cs.todo.model.Project;
import cn.edu.bit.cs.todo.pack.ProjectPack;
import cn.edu.bit.cs.todo.pack.RequestPack;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

//@RunWith(Arquillian.class)
public class RemoteIOTest {
//    @Deployment
//    public static JavaArchive createDeployment() {
//        return ShrinkWrap.create(JavaArchive.class)
//                .addClass(RemoteIO.class)
//                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
//    }

    static RemoteIO remoteIO;

    @BeforeClass
    public static void init() {
//        ServerProcessor serverProcessor = new ServerProcessor("root","passwordW","59.110.233.235",3306,"todo_pro");
//        serverProcessor.launch();
//        String server = "localhost";
        String server = "59.110.233.235";
        remoteIO = new RemoteIO(server, 8899);
    }

    /**
     * 不涉及数据库操作，只针对socket部分
     */
    @Test
    public void sendToRemote() {
        RequestPack requestPack = new RequestPack("testTESTtest");
        String ret = remoteIO.sendToRemote(requestPack.toJson());
        requestPack = RequestPack.createRequestPack(ret);
        assertEquals(true, requestPack.getSuccess());
        assertEquals("TestResult", requestPack.getMsg());
    }

    @Test
    public void addUser() {
        try {
            remoteIO.addUser("test", "test", "testpass");
        } catch (LogicException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void verifyUser() {
        try {
            boolean b = remoteIO.verifyUser("test", "testpass");
            boolean c = remoteIO.verifyUser("test", "testpas");
            assertEquals(true, b);
            assertEquals(false, c);
        } catch (LogicException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void modifyUser() {
        try {
            remoteIO.modifyUser("test", "testpass", "test", "test", "testpass");
        } catch (LogicException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void saveAllProjects() {
        ArrayList<ProjectPack> arrayList = new ArrayList<>();
        arrayList.add(ProjectPack.createProjectPack(new Project("Project-1", "remark1")));
        arrayList.add(ProjectPack.createProjectPack(new Project("Project-2", "remark2")));
        arrayList.add(ProjectPack.createProjectPack(new Project("Project-3", "remark3")));
        try {
            remoteIO.saveAllProjects("test", "testpass", arrayList);
        } catch (LogicException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void getAllProjects() {
        try {
            ArrayList<ProjectPack> arrayList = remoteIO.getAllProjects("test", "testpass");
            for (ProjectPack p : arrayList) {
                System.out.println(p.toJson());
            }
        } catch (LogicException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getUser() {
        try {
            System.out.println(remoteIO.getUser("test", "testpass"));
        } catch (LogicException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getProjecdsUpdateTime() {
        try {
            remoteIO.getProjectsUpdateTime("test", "testpass");
        } catch (LogicException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getDisplayStatus() {
        try {
            assertEquals(false, remoteIO.getDisplayStatus("test"));
            remoteIO.toggleDisplayStatus("test", "testpass");
            assertEquals(true, remoteIO.getDisplayStatus("test"));
            remoteIO.toggleDisplayStatus("test", "testpass");
            assertEquals(false, remoteIO.getDisplayStatus("test"));
        } catch (LogicException e) {
            e.printStackTrace();
        }
    }
}
