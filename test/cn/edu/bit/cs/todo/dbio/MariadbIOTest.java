package cn.edu.bit.cs.todo.dbio;

import cn.edu.bit.cs.todo.exceptions.LogicException;
import cn.edu.bit.cs.todo.pack.ProjectPack;
import cn.edu.bit.cs.todo.pack.RequestPack;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.Assert.*;


public class MariadbIOTest {
    private static MariadbIO dbio;
    private static String host;
    private static int port;

    @BeforeClass
    public static void init() {
        host = "59.110.233.235";
        port = 3306;
        dbio = new MariadbIO("root", "passwordW", host, port, "mariadb", "todo_pro");
        try {
            dbio.addUser("test", "test", "testpass");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getUrl() {
        assertEquals("jdbc:mariadb://" + host + ":" + String.valueOf(port) + "/todo_pro", dbio.getUrl());
    }

    @Test
    public void timeStampStringToLocalDateTime() {
        assertEquals(LocalDateTime.of(2018, 11, 23, 12, 23, 33, 123),
                dbio.timeStampStringToLocalDateTime("2018-11-23 12:23:33.123"));
    }

    @Test
    public void selectUser() throws SQLException {
        assertEquals(1, dbio.selectUser("test", "testpass"));
        assertEquals(0, dbio.selectUser("neverexits", "testpass"));
    }

    @Test
    public void verifyUser() throws SQLException {
        assertEquals(true, dbio.verifyUser("test", "testpass"));
        assertEquals(false, dbio.verifyUser("test", "sssss"));
        assertEquals(false, dbio.verifyUser("neverexits", "sdf"));
    }

    @Test
    public void haveUser() throws SQLException {
        assertEquals(true, dbio.haveUser("test","test"));
        assertEquals(true, dbio.haveUser("neverexits","test"));
        assertEquals(false, dbio.haveUser("neverexits","neverexistz"));
    }

    @Test
    public void addUser() {
        int flag = 0;
        try {
            dbio.addUser("test2", "test", "testpass");

        } catch (SQLException e) {
            flag += 1;
        }

        try {
            dbio.addUser("test", "test2", "testpass");

        } catch (SQLException e) {
            flag += 1;
        }
        assertEquals(2, flag);
    }

    @Test
    public void updateUser() throws SQLException {
        dbio.updateUser(1,"aaaa","bbbb","cccc");
        assertEquals(true,dbio.verifyUser("aaaa","cccc"));
        dbio.updateUser(1,"test","test","testpass");

    }

    @Test
    public void replaceAllProject() {
        ArrayList<ProjectPack> arrayList = new ArrayList<>();
        arrayList.add(ProjectPack.createProjectPack("title1","project1","{}"));
        arrayList.add(ProjectPack.createProjectPack("title2","project3","{{}{}{}{}}"));
        arrayList.add(ProjectPack.createProjectPack("title3","project3","{{}}"));
        dbio.replaceAllProject(1,arrayList);
    }

    @Test
    public void getAllProject() throws SQLException {
        ArrayList<ProjectPack> arrayList = dbio.getAllProject(1);
        assertEquals(3,arrayList.size());
        for (ProjectPack p : arrayList){
            p.toJson();
        }
    }

    @Test
    public void getProjectsUpdateTime() throws SQLException {
        LocalDateTime projectsUpdateTime = dbio.getProjectsUpdateTime(1);
        System.out.printf(projectsUpdateTime.toString());
    }

    @Test
    public void getUser() throws SQLException {
        RequestPack user = null;
        try {
            user = dbio.getUser(1);
        } catch (LogicException e) {
            e.printStackTrace();
        }
        System.out.println(user.toJson());
    }

    @Test
    public void DisplayStatus() {

        try {
            assertEquals(false,dbio.getDisplayStatus(1));
            dbio.toggleDisplayStatus(1);
            assertEquals(true,dbio.getDisplayStatus(1));
            dbio.toggleDisplayStatus(1);
            assertEquals(false,dbio.getDisplayStatus(1));
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (LogicException e) {
            e.printStackTrace();
        }


    }

}
