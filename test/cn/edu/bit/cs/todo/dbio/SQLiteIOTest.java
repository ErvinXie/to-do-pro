package cn.edu.bit.cs.todo.dbio;

import cn.edu.bit.cs.todo.exceptions.LogicException;
import cn.edu.bit.cs.todo.pack.ProjectPack;
import cn.edu.bit.cs.todo.pack.RequestPack;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class SQLiteIOTest {

    static SQLiteIO sqLiteIO;
    static String path = "/home/fky/code/git/mine/to-do-pro/database";
    static String fileName ="todo_pro.db";

    @BeforeClass
    public static void init() {

        sqLiteIO = new SQLiteIO("", "",path
                , "sqlite",fileName
                );
        sqLiteIO.createTable();
    }

    @Test
    public void selectUser() {
        try {
            sqLiteIO.selectUser("test", "test");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void verifyUser() {

    }

    @Test
    public void haveUser() {
        try {
            sqLiteIO.haveUser("test", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void addUser() {

        try {
            sqLiteIO.addUser("test", "test", "testpass");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    @Test
    public void updateUser() {
    }

    @Test
    public void getAllProject() {
        try {
            sqLiteIO.getAllProject(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void replaceAllProject() {
        ArrayList<ProjectPack> arrayList = new ArrayList<>();
        arrayList.add(ProjectPack.createProjectPack("test", "sdfsdf", "{{[]}}"));
        arrayList.add(ProjectPack.createProjectPack("tsetsetset", "sdfsdddddf", "data+ea"));
        arrayList.add(ProjectPack.createProjectPack("ccdfgdfg", "ssss", "data+ad"));

        System.out.println("in the last");
        sqLiteIO.replaceAllProject(1, arrayList);

    }


    @Test
    public void timeStampStringToLocalDateTime() {
        assertEquals(LocalDateTime.of(2018, 11, 23, 12, 23, 33),
                sqLiteIO.timeStampStringToLocalDateTime("2018-11-23 12:23:33"));
    }

    @Test
    public void getUrl() {
        assertEquals("jdbc:sqlite:"+path+"/"+fileName,sqLiteIO.getUrl());
    }

    @Test
    public void getProjectsUpdateTime() throws SQLException {
        LocalDateTime projectsUpdateTime = sqLiteIO.getProjectsUpdateTime(1);
        System.out.printf(projectsUpdateTime.toString());
    }

    @Test
    public void getUser() throws SQLException {
        RequestPack user = null;
        try {
            user = sqLiteIO.getUser(1);
        } catch (LogicException e) {
            e.printStackTrace();
        }
        System.out.println(user.toJson());
    }


    @Test
    public void DisplayStatus() {
        try {
            assertEquals(false,sqLiteIO.getDisplayStatus(1));
            sqLiteIO.toggleDisplayStatus(1);
            assertEquals(true,sqLiteIO.getDisplayStatus(1));
            sqLiteIO.toggleDisplayStatus(1);
            assertEquals(false,sqLiteIO.getDisplayStatus(1));
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (LogicException e) {
            e.printStackTrace();
        }


    }

}
