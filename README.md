# TO-DO-PRO

从零开始实现。
一个基于CS架构的Todo-list管理程序，支持快速共享功能。

[![pipeline status](https://gitlab.com/ErvinXie/to-do-pro/badges/master/pipeline.svg)](https://gitlab.com/ErvinXie/to-do-pro/commits/master)

## 涉及[x] 使用git进行合作开发、版本控制

- [x] 桌面应用开发使用`MVC`架构
- [x] javafx动态生成组件
- [x] 颜色管理系统
- [x] 动画管理系统
- [x] 多用户管理系统
- [x] 列表生成算法设计
- [x] 同步算法设计
- [x] 数据库存储技术
  - [x] 使用Workbench进行数据库设计
  - [x] 同时使用SQLite与Maraidb
  - [x] 支持预处理以防范注入攻击
  - [x] 使用JDBC同时对两种数据库进行操作
- [x] 对象序列化技术
  - [x] Gson
- [x] 多种设计模式使用
  - [x] 工厂模式
  - [x] 建造者模式
  - [x] 注入
- [x] 使用了文件读取、环境变量读取
- [x] Socket编程
- [x] 使用多线程与线程池技术
- [x] 对关键代码应用单元测试（jUnit4）
- [x] 书写了大量文档、并对关键代码生成了javadoc
- [x] 利用gitlab-ci进行可持续集成
- [x] 后端具有较好的OO设计
- [x] Vue.js实现前端
  - [x] 以数据为中心构建前端代码
  - [x] 实现递归的组件渲染
- [x] 利用java实现简单http web服务器
- [x] http服务器较符合restful-api规范
- [x] 后端全部docker容器化
  - [x] linux服务器运维、一台服务器构建docker、另一台部署
  - [x] 使用docker-compose进行容器编排
  - [x] 在java程序中定义了一些环境变量方便部署
  - [x] 进行了数据库数据持久化，数据库数据不会随着新程序的部署而消失

## 实现功能

- [x] 支持用户注册，登陆和同步
- [x] 动态渲染界面
- [x] 颜色管理
- [x] 动画设计
- [x] 远程MariaDB存储同步
- [x] 本地SQLite存储
- [x] 实现支持离线修改，在线同步功能
- [x] Web端快速展示功能

## 项目结构

记录该repo中文件的组织方式

- docs
  - dist
    - 课程要求所需要提交的文档
  - javadoc 存放javadoc
  - `dev-log.md` 一个简要的timeline，记录每次都大概做了什么
  - `discussion.md` 会议记录，杂七杂八的东西
  - `requirement-analysis.md` 需求分析
  - `dev-documentation.md` 开发文档，记录更多设计与规范
- frontend 前端的部署文件与dockerfile
- client-side 客户端
- database mariadb与sql的建表数据
- server-side 服务器端docker
- src 全部的底层代码，全部为手写，会打包成jar
- lib 依赖的jar文件
- test 单元测试代码
- `.gitignore` gitignore
- `.gitlab-ci.yml` 使用gitlab进行可持续集成和DevOps
- `deploy-script.sh` 服务器部署脚本
- `docker-compose.yml` 使用docker-compose进行DevOps与最终部署
- `README.md`
