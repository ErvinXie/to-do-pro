<!-- 开发者文档，定义具体的数据项，书写代码时的参考 -->

# 开发者文档

## 定义

- todolist窗体：本身的用户界面
- project：todolist中有许多

- event：project中的
- subevent:和event相同，只不过event的父节点是project，subevent的父节点是event





## 数据结构

todolist窗体有一个project列表
project列表包含由event、subevent组成的树

树的父子关系表示父事件与子事件
兄弟关系表示先后顺序

## 类型定义

### `AbstractEvent`

描述:project和event的公有部分
继承:NULL

#### 属性

| 属性名称         | 类型             | 描述      | 默认值             | 访问权限    |
|--------------|----------------|---------|-----------------|---------|
| createTime   | java.util.Date | 该任务创建时间 | getTime()       | private |
| title        | String         | 标题      | getDefaultTitle | private |
| remarks      | String         | 备注      | 空               | private |
| isCompleted  | Boolean        | 是否完成此事件 | flase           | private |
| subEventList | ArrayList      | 包含事件列表  | 空               | public  |
|              |                |         |                 |         |

#### 方法

<<<<<<< HEAD
| 方法名称        | 类型           | 描述                                    | 参数           | 返回值        | 备注                           |
| --------------- | -------------- | --------------------------------------- | -------------- | ------------- | ------------------------------ |
| AbstractEvent   | AbstractEvent  | 默认构造函数                            | 无             | AbstractEvent |                                |
| AbstractEvent   | AbstractEvent  | 带标题的构造函数，直接把参数值赋给title | String         | AbstractEvent |                                |
|                 |                |                                         |                |               |                                |
| getCreateTime   | java.util.Date | 获取创建时间                            | 无             | 创建时间      |                                |
| getTitle        | String         | 获取标题                                | 无             | 标题          |                                |
| getRemarks      | String         | 获取备注                                | 无             | 备注          |                                |
| isCompleted     | Boolean        | 获得是否完成                            | 无             | 完成状态      |                                |
|                 |                |                                         |                |               |                                |
| setCreateTime   | Boolean        | 设置创建时间                            | java.util.Date | 是否设置成功  | 或许创建时间不应该被修改？？？ |
| setTitle        | Boolean        | 设置事件标题                            | String         | 是否设置成功  |                                |
| setRemarks      | Boolean        | 设置备注                                | String         | 是否设置成功  |                                |
|                 |                |                                         |                |               |                                |
| addSubEvent     | Boolean        | 添加子事件                              | Event          | 是否添加成功  | 到最后                         |
| addSubEvent     | Boolean        | 添加子事件                              | Event, int x   | 是否添加成功  | 插入后下标变为x                |
|                 |                |                                         |                |               |                                |
| complete        | Boolean        | 完成此事件                              | 无             | 是否操作成功  |                                |
| deComplete      | Boolean        | 取消完成此事件                          | 无             | 是否操作成功  |                                |
|                 |                |                                         |                |               |                                |
| Abstract delete | Boolean        | 删除此事件                              | 无             | 是否删除成功  | 抽象函数                       |
|                 |                |                                         |                |               |                                |
| toString        | String         | json化                                  | 无             | json字符串    |                                |
|                 |                |                                         |                |               |                                |
=======
| 方法名称          | 类型             | 描述                     | 参数             | 返回值           | 备注              |
|---------------|----------------|------------------------|----------------|---------------|-----------------|
| AbstractEvent | AbstractEvent  | 默认构造函数                 | 无              | AbstractEvent |                 |
| AbstractEvent | AbstractEvent  | 带标题的构造函数，直接把参数值赋给title | String         | AbstractEvent |                 |
|               |                |                        |                |               |                 |
| getCreateTime | java.util.Date | 获取创建时间                 | 无              | 创建时间          |                 |
| getTitle      | String         | 获取标题                   | 无              | 标题            |                 |
| getRemarks    | String         | 获取备注                   | 无              | 备注            |                 |
| isCompleted   | Boolean        | 获得是否完成                 | 无              | 完成状态          |                 |
|               |                |                        |                |               |                 |
| setCreateTime | Boolean        | 设置创建时间                 | java.util.Date | 是否设置成功        | 或许创建时间不应该被修改？？？ |
| setTitle      | Boolean        | 设置事件标题                 | String         | 是否设置成功        |                 |
| setRemarks    | Boolean        | 设置备注                   | String         | 是否设置成功        |                 |
|               |                |                        |                |               |                 |
| addSubEvent   | Boolean        | 添加子事件                  | Event          | 是否添加成功        | 到最后             |
| addSubEvent   | Boolean        | 添加子事件                  | Event, int x   | 是否添加成功        | 插入后下标变为x        |
|               |                |                        |                |               |                 |
| complete      | Boolean        | 完成此事件                  | 无              | 是否操作成功        |                 |
| deComplete    | Boolean        | 取消完成此事件                | 无              | 是否操作成功        |                 |
|               |                |                        |                |               |                 |
| delete        | Boolean        | 删除此事件                  | 无              | 是否删除成功        |                 |
|               |                |                        |                |               |                 |
| toString      | String         | json化                  | 无              | json字符串       |                 |
|               |                |                        |                |               |                 |
>>>>>>> master


### `Project`

描述:一个project
继承:`AbstractEvent`

#### 字段

| 属性名称      | 类型     | 描述                        | 默认值               | 访问权限    | 备注             |
|-----------|--------|---------------------------|-------------------|---------|----------------|
| projectId | String | 防止相同project名冲突，使用一个唯一id标识 | md5(current_time) | private | 数据库操作的时候需要用到这个 |
|           |        |                           |                   |         |                |
|           |        |                           |                   |         |                |
|           |        |                           |                   |         |                |
|           |        |                           |                   |         |                |

#### 方法

| 方法名称    | 类型      | 描述                   | 参数     | 返回值     |
|---------|---------|----------------------|--------|---------|
| Project | Project | 默认构造函数，直接调用父类对应的构造函数 | 无      | Project |
| Project | Project | 设置标题的构造函数            | String | Project |
| Project | Project | 设置标题和开始时间的构造函数       |        | Project |
|         |         |                      |        |         |
|         |         |                      |        |         |
|         |         |                      |        |         |
|         |         |                      |        |         |

### `Event`

描述:一个Event
继承:`AbstructEvent`

#### 字段

| 属性名称      | 类型             | 描述          | 默认值                | 访问权限    |  |
|-----------|----------------|-------------|--------------------|---------|--|
| startTime | java.util.Date | 该任务的开始时间    | getTempStartTime() | private |  |
| endTime   | java.util.Date | 该任务结束时间     | startTime+1h       | private |  |
| priority  | int            | 任务优先级，由用户定义 | 6                  | private |  |
|           |                |             |                    |         |  |
|           |                |             |                    |         |  |
|           |                |             |                    |         |  |
|           |                |             |                    |         |  |
|           |                |             |                    |         |  |
|           |                |             |                    |         |  |

#### 方法

| 方法名称         | 类型      | 描述           | 参数   | 返回值           | 备注   |
|--------------|---------|--------------|------|---------------|------|
| :----------: | :-----: | :----------: | :--: | :-----------: | :--: |
| getStartTime | Date    | 获取开始时间       | 无    | 开始时间          |      |
| getEndTime   | Date    | 获取结束时间       | 无    | 结束时间          |      |
| getPriority  | int     | 获取优先级        | 无    | 优先级           |      |
|              |         |              |      |               |      |
| setStartTime | Boolean | 设置开始时间       | Date | 是否设置 成功       |      |
| setEndTime   | Boolean | 设置结束时间       | Date | 是否设置成功        |      |
| setPriority  | Boolean | 设置优先级        | int  | 是否设置成功        |      |
|              |         |              |      |               |      |
|              |         |              |      |               |      |
|              |         |              |      |               |      |
|              |         |              |      |               |      |
|              |         |              |      |               |      |
|              |         |              |      |               |      |

## 数据库设计

### `users`

| 字段名                                                   | 类型           | 描述            | 修饰                         | 默认值                                                   | 备注      |
|-------------------------------------------------------|--------------|---------------|----------------------------|-------------------------------------------------------|---------|
| uid                                                   |              | 主键            | AUTO_INCREMENT PRIMARY KEY |                                                       |         |
| username                                              | VARCHAR(128) |               | NOT NULL  UNIQUE           |                                                       |         |
| email                                                 | VARCHAR(20)  |               | UNIQUE                     |                                                       |         |
| password                                              | CHAR(32)     |               | NOT NULL                   |                                                       |         |
| updateTime                                            | DATETIME     | 更新时间          |                            | DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP |         |
| DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP |              |               |                            |                                                       |         |
| projectsUpdateTime                                    | DATETIME     | projects表更新时间 |                            | DEFAULT CURRENT_TIMESTAMP                             | 使用`触发器` |

### `projects`

| 字段名        | 类型           | 描述          | 修饰                                    | 默认值 | 备注 |
|------------|--------------|-------------|---------------------------------------|-----|----|
| id         | INT          | 主键          | AUTO_INCREMENT PRIMARY KEY            |     |    |
| uid        | INT          | 属于哪个用户      | FOREIGN KEY(uid)REFERENCES users(uid) |     |    |
| title      | VARCHAR(100) | 名称          | NOT NULL                              |     |    |
| project_id | char(32)     | 项目的id，由用户传输 |                                       |     |    |
| json       | TEXT         | json数据      |                                       |     |    |
| updateTime | DATETIME     | 更新时间        |                                       |     |    |

### `DatabaseIO`

#### 方法

| 方法名称              | 返回类型                    | 描述                    | 参数                                    | 返回值     |
|-------------------|-------------------------|-----------------------|---------------------------------------|---------|
| selectUser        | int                     | 返回uid，若无则返回0          | userOrEmail, password                 | uid 或 0 |
| addUser           | void                    | 维护持续时间的值              | username, email, password             |         |
| haveUesr          | boolean                 |                       | userOrEmail                           | 是否存在该用户 |
| updateUesr        | void                    | 更新用户的用户名，邮箱，密码        | uid, username, email, password        | 是否存在该用户 |
| verifyUser        | boolean                 | 查找是否合法,对selectUser的封装 | userOrEmail, password                 |         |
| getAllProject     | ArrayList\<ProjectPack> | 获取所有project           | uid                                   |         |
| replaceAllProject | void                    | 存储所有数据                | int uid, ArrayList<ProjectPack> array |         |


## 网络传输api

### 存储

```json
// 存储
socket
{
    "uname":"username",
    "upass":"PASSWORD-HASH",
    "action":"verifyUser|addUser|modifyUser|saveAll|getAll|getUser|getProjectsUpdateTime"
    // 存储数据
    "jsonData":[
        {
            "title" : "PROJECTNAME",
            "projectId":234,
            "data":"json_format_data"

        },
        {
            "title" : "another PROJECTNAME",
            "projectId":234,
            "data":"another json_format_data"

        }
    ]
}

// response
{
    "success":true,
    "msg":"" // 如果返回值为false 则有msg内容
}


// 获取全部project
socket
{
    "upass":"PASSWORD-HASH",
}

// response
{
    "success":true,
    "jsonData":[
        {
            "title" : "PROJECTNAME",
            "projectId":234,
            "data":"json_format_data",
            "updateTime":""

        },
        {
            "title" : "another PROJECTNAME",
            "projectId":234,
            "data":"another json_format_data",
            "updateTime":""
        }
    ], // 数据
    "msg":"" // 如果返回值为false则有内容
}

```

具体的实现通过`RequestPack类`来实现
采用这个类来表示所有通信数据，而利用Gson，只有初始化不为null的部分才会出现在最终的json里。


## 同步代码调用接口 Syncer

具体接口已在javadoc中写明

```java

private static Type name() {
    Syncer syncer = new syncer("1.2.33.444",8899,"./","todo_pro.db");

    sycner.add("test","email","password");

    //  登录时会同步，并且记忆密码，这样保证之后的projects操作不需要传入密码
    sycner.login("test","password");

    syncer.saveAllProjects(projectsArrayList);

    projectsArrayList = syncer.getAllProjects();
}

```

## 

