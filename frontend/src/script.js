var sourceOfRemark = {
  debug: true,
  state: {
    value: ''
  },
  setValueAction(newValue) {
    if (this.debug) console.log('setValueAction triggered with', newValue)
    this.state.value = newValue
  }
}
// var mydata = [{
//   'title': 'project1',
//   'remarks': 'test project',
//   'isCompleted': false,
//   'subEventList': [{
//     'title': 'event1',
//     'remarks': 'today1',
//     'isCompleted': false,
//     'subEventList': []
//   }, {
//     'title': 'event2',
//     'remarks': 'today2',
//     'isCompleted': true,
//     'subEventList': []
//   }, {
//     'title': 'event3',
//     'remarks': 'today333',
//     'isCompleted': false,
//     'subEventList': []
//   }]
// }, {
//   'title': '这是第二个project',
//   'remarks': 'test project',
//   'isCompleted': false,
//   'subEventList': [{
//     'title': 'event11',
//     'remarks': 'todaaay',
//     'isCompleted': true,
//     'subEventList': []
//   }, {
//     'title': 'event22',
//     'remarks': 'todaytt',
//     'isCompleted': false,
//     'subEventList': []
//   }, {
//     'title': 'event33',
//     'remarks': 'todayasadf',
//     'isCompleted': false,
//     'subEventList': [{
//       'title': 'event44',
//       'remarks': 'today',
//       'isCompleted': false,
//       'subEventList': []
//     }]
//   }]
// }, {
//   'title': 'project 3',
//   'remarks': 'test project',
//   'isCompleted': false,
//   'subEventList': [{
//     'title': 'event11',
//     'remarks': 'todaaay',
//     'isCompleted': true,
//     'subEventList': []
//   }, {
//     'title': 'event22',
//     'remarks': 'todaytt',
//     'isCompleted': false,
//     'subEventList': []
//   }, {
//     'title': 'event33',
//     'remarks': 'todayasadf',
//     'isCompleted': false,
//     'subEventList': [{
//       'title': 'event44',
//       'remarks': 'today',
//       'isCompleted': false,
//       'subEventList': []
//     }]
//   }]
// },
//   {
//     'title': 'project 6',
//     'remarks': 'test project',
//     'isCompleted': false,
//     'subEventList': [{
//       'title': 'event11',
//       'remarks': 'todaaay',
//       'isCompleted': true,
//       'subEventList': []
//     }, {
//       'title': 'event22',
//       'remarks': 'todaytt',
//       'isCompleted': false,
//       'subEventList': []
//     }, {
//       'title': 'event33',
//       'remarks': 'todayasadf',
//       'isCompleted': false,
//       'subEventList': [{
//         'title': 'event44',
//         'remarks': 'today',
//         'isCompleted': false,
//         'subEventList': []
//       }]
//     }]
//   }

// ]

// define the item component
Vue.component('item', {
  template: '#item-template',
  props: {
    model: Object,
    remark: String
  },
  data: function () {
    return {
      open: false
    }
  },
  computed: {
    isFolder: function () {
      return this.model.subEventList &&
        this.model.subEventList.length
    },
    isCompleted: function () {
      return this.model.isCompleted
    }
  },
  methods: {
    toggle: function () {
      if (this.isFolder) {
        this.open = !this.open
      }
    },
    info: function (e) {
      console.log(e)
      console.log(sourceOfRemark)
      sourceOfRemark.setValueAction(e)
    // this.$emit('change-dis', e)
    }
  }
})

// boot up the demo
var demo = new Vue({
  el: '#demo',
  data: {
    author: user,
    treeData:{},
    myData: [],
    remark: sourceOfRemark.state,
    isActive: 0
  },
  created: function () {
    var url = 'json/' + user
    var _self = this
    $.getJSON(
      url,
      function (data) {
        // _self.data = eval("(" + data + ")")
        // console.log('this tse')
        _self.myData = data
        _self.treeData = data[0]
        // console.log(typeof data)
        // console.log(data)
      },
     )
  /*
  this.$http.get(url).then(function(data){
      var json=data.body
      this.data=eval("(" + json +")")
  },function(response){
      console.info(response)
  })*/
  },
  methods: {
    select: function (a, b) {
      this.treeData = this.myData[b]
      this.isActive = b
    },
  // changeDis: function (mod, a, b) {
  //     sourceOfRemark = mod
  //     console.log(mod)
  //     console.log(a)
  //     console.log(b)
  // }
  }
})
